# -*- coding: utf-8 -*-
"""
Created on Tue Aug  1 16:02:23 2023

@author: Titouan
"""

import numpy as np

# =============================================================================
# Règles de décisions utilisées
# =============================================================================


def create_decision(mydf, threshold_hvb):
    decision01 = 0
    for myyear in range(12, 20):
        if mydf["results" + str(myyear)] > threshold_hvb :
            decision01 = 1
    return decision01

def newrule(mydf, newthresh):
    yearmax = int(mydf["year_max"])
    decision01 = 0
    typedecision = int(mydf["fibrose_F4_B"])
    if (typedecision == 1):
        for myyear in range(12, 20):
            if mydf["results" + str(myyear)] > newthresh :
                decision01 = 1
    if (typedecision == 0):    
        for myyear in range(12, yearmax):
            if mydf["results" + str(myyear)] > newthresh :
                decision01 = 1
    return decision01


def rule_single(mydf, newthresh):
    yearmax = int(mydf["year_incl"])
    decision01 = 0
    if mydf["results" + str(yearmax-2000)] > newthresh :
        decision01 = 1
    return decision01


def rule_beforeincl(mydf, cat_target, newthresh):
    
    if cat_target in ["fibrose_F4_B", "fibrose_F4_C"]:
        date_used = "year_fibrose"
    
    else:
        date_used = "year_incl"
        
    yearmax = int(mydf[date_used])-2000
    decision01 = 0
    for myyear in range(12, yearmax+1):
        if mydf["results" + str(myyear)] > newthresh :
            decision01 = 1
            
    return decision01

def rule_triolet(mydf, cat_target, newthresh):
    
    if cat_target in ["fibrose_F4_B", "fibrose_F4_C"]:
        date_used = "year_incl"
    
    else:
        date_used = "year_incl"
        
    yearmax = int(mydf[date_used])-2000
    decision01 = 0
    try:
        for myyear in range(yearmax-1, yearmax+2):
            if mydf["results" + str(myyear)] > newthresh :
                decision01 = 1
    except:
        for myyear in range(yearmax, yearmax+2):
            if mydf["results" + str(myyear)] > newthresh :
                decision01 = 1
    return decision01

def rule_allspan(mydf, newthresh):
    decision01 = 0
    for myyear in range(13, 19):
        if mydf["results" + str(myyear)] > newthresh :
            decision01 = 1
    return decision01

def count_allspan(mydf, newthresh):
    count01 = 0
    for myyear in range(12, 20):
        if mydf["results" + str(myyear)] > newthresh :
            count01 += 1
    return count01

def get_max_quant(myrow):
    myvalues = np.array['results12', 
                        'results13', 
                        'results14',
                        'results15', 
                        'results16', 
                        'results17', 
                        'results18', 
                        'results19']
    return np.max(myvalues)

    