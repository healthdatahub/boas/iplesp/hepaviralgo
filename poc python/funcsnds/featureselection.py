# -*- coding: utf-8 -*-
"""
Fonctions pour trouver des variables pertinentes dans le SNDS pour prédire les 
phénotypes d'intérêt. Un bloc est spécifique aux ALD issues de IR_IMB_R car
l'extraction du SNDS dispose d'une table par année pour le DCIR et le MCO mais 
toutes les ALD sont dans un fichier unique pour l'ensemble de la période
considérée
"""

# =============================================================================
# Packages utilisées
# =============================================================================


import pandas as pd
import numpy as np

from sklearn.linear_model import Lasso
from sklearn.utils import resample
from datetime import datetime

# Prétraitements standard du SNDS
import funcsnds.featurecleaning as fc

# Variables globales au projet (clefs de jointures, variables de filtres)
import funcsnds.globalvar as gv

# A importer pour travailer avec polars plutôt que pandas
# import polars as pl
# import pyarrow as pa

dic_join_vars = gv.dic_join_vars

# =============================================================================
# Fonctions de base de la feature selection
# =============================================================================


def import_databases(type_snds : str, 
                     path_snds : str, 
                     path_affine: str, 
                     var_restricted: list[str], 
                     year: int) -> pd.DataFrame:
    """
    Fonction pour importer une table unique d'une année unique. Les structures 
    du PMSI et du DCIR étant distincts les importations diffèrent légèrement

    Parameters
    ----------
    type_snds : str
        "Type" de SNDS étudié, choix "DCIR", "MCO" ou "SSR" pour le moment.
    path_snds : str
        Chemin du SNDS local pour le travail à l'IPLESP.
    path_affine : str
        Nom de la table affiné. 
        Si "None" alors on obtient les tables respectives "principales" :
            - "DCIR" + "None" => "ER_PRS_F"
            - "MCO"  + "None" => "T_MCOaaB"
            - "SSR"  + "None" => "T_SSRaaB"
    var_restricted : list[str]
        Optionnel : liste pour importer un nombre limité de variable qui seront 
        binarisées puis prises en compte dans le Lasso.
        Si égal à "None" importe toutes les variables disponibles
        Ceci afin de limiter le temps computationnel, mais aussi de ne pas 
        avoir de variables "overfittés"
    year : int
        Année d'intérêt à importer.

    Returns
    -------
    df_snds : pd.DataFrame
        Table affinée ou "principale" du SNDS pour une année donnée, avec une 
        sélection de variable préliminaire éventuelle

    """
    
    join_vars = dic_join_vars[type_snds]
    
    # Importation des tables du DCIR
    if (type_snds == "DCIR"):
        file_er_prs_f =  path_snds + "ER_PRS_F_20" + str(year) + ".csv"
        
        if (path_affine == None): # Importe ER_PRS_F
            var_prs_f = var_restricted + ["NUM_ENQ", "CPL_MAJ_TOP", "DPN_QLF"]
            df_snds_raw = pd.read_csv(file_er_prs_f, 
                                      sep = ";", 
                                      usecols = var_prs_f,
                                      dtype = "object")    
            df_snds = fc.clean_erprsf(df_snds_raw).copy()
            df_snds.fillna("MISSING", inplace = True) 
            
        else: # Importe table affinée du DCIR
            df_snds = pd.read_csv(file_er_prs_f, 
                                  sep = ";", 
                                  usecols = (join_vars + ["NUM_ENQ"]),
                                  dtype = "object")
            
            # Exemple si l'on voulait utiliser polars
            # df_snds = pl.read_csv(file_er_prs_f, 
            #                       separator = ";", 
            #                       columns = (join_vars + ["NUM_ENQ"]))
            # df_snds = df_snds.to_pandas()
            
            file_affine =  path_snds + path_affine + "_20" + str(year) + ".csv"
            
            # Erreur d'encodage spécifique à l'IPLESP pour des tables ER_CAM_F
            if (path_affine == "ER_CAM_F"):
                df_affine_raw = pd.read_csv(file_affine, 
                                            sep = ";", 
                                            usecols = 
                                            (join_vars + var_restricted), 
                                            dtype = "object", 
                                            encoding_errors="ignore")
            else:
                df_affine_raw = pd.read_csv(file_affine, 
                                            sep = ";", 
                                            usecols = 
                                            (join_vars + var_restricted), 
                                            dtype = "object")
                
            df_affine = fc.clean_dcir_affine(df_affine_raw).copy()
            df_affine.fillna("MISSING", inplace = True)
            df_snds = pd.merge(left = df_snds, 
                               right = df_affine, 
                               how = "inner", 
                               on = join_vars)
    
    # Importations quasi similaires pour PMSI : MCO et SSR
    if (type_snds == "MCO"):

        file_intermediate = path_snds + "T_" + type_snds + str(year) + "C.csv"
        file_aaB = path_snds + "T_" + type_snds + str(year) + "B.csv"
        df_intermediate = pd.read_csv(file_intermediate, 
                                      sep = ";", 
                                      usecols = (join_vars + ["NUM_ENQ"]), 
                                      dtype = "object")
        
        if (path_affine == None): # Importe T_MCO/SSRaaB
            df_snds_raw = pd.read_csv(file_aaB, 
                                      sep = ";", 
                                      usecols = (var_restricted + join_vars), 
                                      dtype = "object")
            
            df_snds = fc.clean_pmsi_aaB(df_snds_raw, type_snds).copy()
            df_snds.fillna("MISSING", inplace = True) 
            df_snds = pd.merge(left = df_snds, 
                               right = df_intermediate, 
                               how = "inner", 
                               on = join_vars)
            
        else: # Importe table affinée des PMSI
            
            df_snds_raw = pd.read_csv(file_aaB, 
                                      sep = ";", 
                                      usecols = join_vars, 
                                      dtype = "object")
            
            df_snds = fc.clean_pmsi_aaB(df_snds_raw, type_snds).copy()
            
            df_snds = pd.merge(left = df_snds, 
                               right = df_intermediate, 
                               how = "inner", 
                               on = join_vars)
            
            file_affine = path_snds+"T_"+type_snds+str(year)+path_affine+".csv"
            
            df_affine_raw = pd.read_csv(file_affine,
                                        sep = ";", 
                                        usecols = (join_vars + var_restricted), 
                                        dtype = "object")
            
            df_affine = fc.clean_pmsi_affine(df_affine_raw).copy()
            
            df_affine.fillna("MISSING", inplace = True)
            
            df_snds = pd.merge(left = df_snds, 
                               right = df_affine, 
                               how = "inner", 
                               on = join_vars)

                
    if (type_snds == "SSR"):
        
        join_vars_rhs = dic_join_vars["SSR_affine"]
        
        file_intermediate = path_snds + "T_" + type_snds + str(year) + "C.csv"
        
        file_aaB = path_snds + "T_" + type_snds + str(year) + "B.csv"
        
        df_intermediate = pd.read_csv(file_intermediate, 
                                      sep = ";", 
                                      usecols = (join_vars + ["NUM_ENQ"]), 
                                      dtype = "object")
        
        if (path_affine == None): # Importe T_MCO/SSRaaB
            df_snds_raw = pd.read_csv(file_aaB, 
                                      sep = ";", 
                                      usecols = (var_restricted + join_vars_rhs), 
                                      dtype = "object")
            
            df_snds = fc.clean_pmsi_aaB(df_snds_raw, type_snds).copy()
            df_snds.fillna("MISSING", inplace = True) 
            
            df_snds = pd.merge(left = df_snds, 
                               right = df_intermediate, 
                               how = "inner", 
                               on = join_vars)
            
            df_snds = df_snds.drop("RHS_NUM", axis = 1).drop_duplicates()
            
        else: # Importe table affinée des PMSI
            
            df_snds_raw = pd.read_csv(file_aaB, 
                                      sep = ";", 
                                      usecols = join_vars_rhs, 
                                      dtype = "object")
            
            df_snds = fc.clean_pmsi_aaB(df_snds_raw, type_snds).copy()
            
            df_snds = pd.merge(left = df_snds, 
                               right = df_intermediate, 
                               how = "inner", 
                               on = join_vars)
            
            file_affine = path_snds+"T_"+type_snds+str(year)+path_affine+".csv"
            
            # Rôle particulier dans le SSR où l'on décompose au niveau semaine
            
            try:
                df_affine_raw = pd.read_csv(file_affine,
                                            sep = ";", 
                                            usecols = (join_vars_rhs + var_restricted), 
                                            dtype = "object")
                
                df_affine = fc.clean_pmsi_affine(df_affine_raw).copy()
                
                df_affine.fillna("MISSING", inplace = True)
                
                df_snds = pd.merge(left = df_snds, 
                                   right = df_affine, 
                                   how = "inner", 
                                   on = join_vars_rhs)
                
                # On supprime la démultiplication très fréquentes des ALD dans 
                # le SSR car chaque séjour est dupliqué au niveau hebdomadaire
                df_snds = df_snds.drop("RHS_NUM", axis = 1).drop_duplicates()
                
            except:
                df_affine_raw = pd.read_csv(file_affine,
                                            sep = ";", 
                                            usecols = (join_vars + var_restricted), 
                                            dtype = "object")
                
                df_affine = fc.clean_pmsi_affine(df_affine_raw).copy()
                
                df_affine.fillna("MISSING", inplace = True)
                
                df_snds = pd.merge(left = df_snds, 
                                   right = df_affine, 
                                   how = "inner", 
                                   on = join_vars)
            
    return df_snds


def flatten_snds(df_snds: pd.DataFrame, 
                 var_restricted: list[str]) -> pd.DataFrame:
    """
    Transforme les tables SNDS (format long) en tables interprétables par un
    algorithme de machine learning (format wide) : une ligne par individu au 
    lieu d'une ligne par évènement

    Parameters
    ----------
    df_snds : pd.DataFrame
        Table au format long (une ligne = un évènement), issue de l'importation 
        brute des tables SNDS, à transformer
    var_restricted : list[str]
        Liste des variables restrictives, limite le temps computationnel

    Returns
    -------
    flat_df : pd.DataFrame
        DESCRIPTION.

    """
    
    # Initialisation avec les identifiants
    flat_df = pd.DataFrame(df_snds["NUM_ENQ"].unique(), 
                             columns = ["NUM_ENQ"])
    
    for myvar in var_restricted:
        flat_temp = df_snds[["NUM_ENQ"]].join(
            pd.get_dummies(data = df_snds[myvar], 
                           prefix = myvar,
                           prefix_sep= "/")).groupby("NUM_ENQ").sum()
        
        flat_df = pd.merge(left = flat_df, 
                             right = flat_temp, 
                             how = "inner", 
                             on = "NUM_ENQ")
    return flat_df   


def merge_status(flat_df: pd.DataFrame, 
                 cat_target: str) -> pd.DataFrame:
    """
    Ajoute à la table "aplati" du SNDS le statut "gold standard" issu de la
    cohorte HEPATHER.

    Parameters
    ----------
    flat_df : pd.DataFrame
        Table affinée aplatie (une ligne = un individu).
    cat_target : str
        Phénotype étudié.

    Returns
    -------
    data : pd.DataFrame
        DESCRIPTION.

    """
    
    # La variable phenotype_champ restreint l'analyse a un sous champ.
    # En effet on ne cherche par exemple à trouver les hépatites Delta qu'au
    # sein des HVB chroniques et non l'ensemble de la cohorte HEPATHER.
    champ = cat_target + "_champ"
    snds_hepather = pd.read_csv("data/chainage_snds_statut_hepather.csv", 
                                usecols = ["NUM_ENQ", champ, cat_target])
    snds_hepather.drop_duplicates(inplace = True)
    data = pd.merge(left = flat_df, 
                    right = snds_hepather, 
                    on = "NUM_ENQ", 
                    how = "inner")
    
    data = data[data[champ] == 1].copy()
    
    return data
    

def resample_data(data: pd.DataFrame, 
                  cat_target: str) -> pd.DataFrame:
    """
    Resampling de l'échantillon, nécessaire car de nombreux phénotypes entraîne
    un travail de classification déséquilibré

    Parameters
    ----------
    data : pd.DataFrame
        Données au format compréhensible par sklearn, mais déséquilibré.
    cat_target : str
        Phénotype étudié.

    Returns
    -------
    data_resampled : pd.DataFrame
        Données prêtes pour la régression Lasso

    """
    
    data1 = data[data[cat_target] == 1]
    data2 = data[data[cat_target] != 1]
    
    if (data1.shape[0] >= data2.shape[0]):
        data1_upsample = resample(data1,
                                  replace = True,
                                  n_samples = data2.shape[0])    
        data_resampled = pd.concat([data1_upsample, data2])
        
    else:
        data2_upsample = resample(data2,
                                  replace = True,
                                  n_samples = data1.shape[0])    
        data_resampled = pd.concat([data1, data2_upsample])
        
    return data_resampled


def top_predictors(alpha: float, 
                   data_resampled: pd.DataFrame, 
                   cat_target: str) -> pd.DataFrame:
    """
    Calcul les meilleurs prédicteurs pour un phénotype donné, trié par ordre
    d'importance (valeur du coefficient de la régression Lasso)

    Parameters
    ----------
    alpha : float
        Paramètre du Lasso, si faible augmente le nombres de variables choisies
    data_resampled : pd.DataFrame
        Données au format compréhensible par sklearn, mais déséquilibré.
    cat_target : str
        Phénotype étudié.

    Returns
    -------
    results : pd.DataFrame
        Résultats de la régression Lasso

    """
    
    champ = cat_target + "_champ"
    target = data_resampled[[cat_target]]
    data_x = data_resampled.drop([champ, cat_target], axis = 1)
    model = Lasso(alpha)
    _ = model.fit(data_x, target)
    ranking = []
    for name, coef in zip(data_resampled.columns, model.coef_):
        ranking.append([name, np.abs(coef)])
    results = pd.DataFrame(ranking, columns = 
                           ["Var", "Factor"]).sort_values(by = "Factor", 
                                                          ascending = False)
    return results


def summarize_rank_years(df_years: pd.DataFrame) -> pd.DataFrame:
    """
    Trie la table de résultats de plusieurs années
    
    Parameters
    ----------
    df_years : pd.DataFrame
        Table agrégeant les résultats Lasso sur plusieurs années

    Returns
    -------
    results_ranked : TYPE
        Table triée avec vars les plus importantes à travers les années

    """
    
    Factor_Total = 0
    df_years_filled = df_years.fillna(0)
    for (colname, colval) in df_years_filled.iteritems():
        if colname == "Var":
            pass
        else:
            Factor_Total += colval.values
    df_years["Factor_Total"] = Factor_Total

    results_ranked = df_years.sort_values("Factor_Total", ascending = False)
    
    return results_ranked  


def get_path_lasso(path_affine: str, 
                   cat_target: str, 
                   type_snds: str,
                   wotp = "") -> str:
    """
    Retrieve path of lasso regression with top predictors. Used for WOTP
    find_feature in order to get other relevant variables.

    Parameters
    ----------
    path_affine : str
        Table considérée.
    cat_target : str
        Phénotype étudié.
    type_snds : str
        Type de SNDS.

    Returns
    -------
    str
        Chemin vers le fichier Lasso.

    """
    path_lasso = "results/" + cat_target + "/lasso/lasso_" + wotp
    if (path_affine == None):
        if (type_snds == "DCIR"):
            path_lasso += "ER_PRS_F.csv"
        else:
            path_lasso += type_snds + "aaB.csv"
    else:
        if (type_snds == "DCIR"):
            path_lasso += path_affine + ".csv"
        else:
            path_lasso += type_snds + "aa" + path_affine + ".csv"
    
    return path_lasso


# =============================================================================
# Agrégation des fonctions pour utilisation dans notebook Jupyter
# =============================================================================


def find_features(type_snds: str, 
                  cat_target: str, 
                  path_snds: str, 
                  path_affine: str, 
                  var_restricted: list[str], 
                  year: int, 
                  alpha: float) -> pd.DataFrame:
    """
    Donne les meilleures variables prédictrices du phénotype étudie, une année 
    donnée pour une table spécifique du SNDS.

    Parameters
    ----------
    type_snds : str
        "Type" de SNDS étudié, choix "DCIR", "MCO" ou "SSR" pour le moment.
    cat_target : str
        Phénotype étudié (VHB chronique, VHC chronique, cirrhose, etc)
    path_snds : str
        Chemin du SNDS local pour le travail à l'IPLESP.
    path_affine : str
        Nom de la table affiné. 
    var_restricted : list[str]
        Optionnel : liste pour importer un nombre limité de variable qui seront 
        binarisées puis prises en compte dans le Lasso.
    year : int
        Année d'intérêt à importer.
    alpha : float
        Paramètre régularisateur de la régression Lasso, plus le coefficient
        est faible plus on aura de variables retenues

    Returns
    -------
    results : pd.DataFrame
        Tableau trié donnant les meilleures variables prédictrices

    """
    df_snds = import_databases(type_snds, 
                               path_snds, 
                               path_affine, 
                               var_restricted, 
                               year)
    flat_df = flatten_snds(df_snds, var_restricted)
    # CHANGEMENT
    flat_df = flat_df.loc[:,~flat_df.columns.str.endswith('MISSING')]
    data = merge_status(flat_df, cat_target)
    data_resampled = resample_data(data.drop("NUM_ENQ", axis = 1), cat_target)
    results = top_predictors(alpha, data_resampled, cat_target)
    
    return results



def find_features_wotoppredictors(type_snds: str, 
                                  cat_target: str, 
                                  path_snds: str, 
                                  path_affine: str, 
                                  var_restricted: list[str], 
                                  year: int, 
                                  alpha: float,
                                  topexcluded = 10) -> pd.DataFrame:
    """
    Donne les meilleures variables prédictrices du phénotype étudie, une année 
    donnée pour une table spécifique du SNDS.
    WOTP = WithOut Top Predictors, il s'agit d'un fonction spécifiqu

    Parameters
    ----------
    type_snds : str
        "Type" de SNDS étudié, choix "DCIR", "MCO" ou "SSR" pour le moment.
    cat_target : str
        Phénotype étudié (VHB chronique, VHC chronique, cirrhose, etc)
    path_snds : str
        Chemin du SNDS local pour le travail à l'IPLESP.
    path_affine : str
        Nom de la table affiné. 
    var_restricted : list[str]
        Optionnel : liste pour importer un nombre limité de variable qui seront 
        binarisées puis prises en compte dans le Lasso.
    year : int
        Année d'intérêt à importer.
    alpha : float
        Paramètre régularisateur de la régression Lasso, plus le coefficient
        est faible plus on aura de variables retenues

    Returns
    -------
    results : pd.DataFrame
        Tableau trié donnant les meilleures variables prédictrices

    """
    df_snds = import_databases(type_snds, 
                               path_snds, 
                               path_affine, 
                               var_restricted, 
                               year)
    flat_df = flatten_snds(df_snds, var_restricted)
    # CHANGEMENT
    flat_df = flat_df.loc[:,~flat_df.columns.str.endswith('MISSING')]
    data = merge_status(flat_df, cat_target)
    
    # Modification pour retirer les top 10 meilleurs prédicteurs
    import_temp = pd.read_csv(get_path_lasso(path_affine, 
                                             cat_target, 
                                             type_snds))
    wotoppredictors = list(import_temp["Var"].head(topexcluded))
    wotoppredictors = list(set(data.columns).intersection(set(wotoppredictors)))
    data.drop(wotoppredictors, axis = 1, inplace = True)
    
    data_resampled = resample_data(data.drop("NUM_ENQ", axis = 1), cat_target)
    results = top_predictors(alpha, data_resampled, cat_target)
    
    return results


def find_features_allyears(type_snds: str, 
                          cat_target: str, 
                          path_snds: str, 
                          path_affine: str, 
                          var_restricted: list[str], 
                          year_max: int, 
                          alpha: float) -> pd.DataFrame:
    """
    Donne les meilleures variables prédictrices du phénotype étudie, en 
    utilisant toutes les années jusqu'à year_max et en les triant

    Parameters
    ----------
    type_snds : str
        "Type" de SNDS étudié, choix "DCIR", "MCO" ou "SSR" pour le moment.
    cat_target : str
        Phénotype étudié (VHB chronique, VHC chronique, cirrhose, etc)
    path_snds : str
        Chemin du SNDS local pour le travail à l'IPLESP.
    path_affine : str
        Nom de la table affiné. 
    var_restricted : list[str]
        Optionnel : liste pour importer un nombre limité de variable qui seront 
        binarisées puis prises en compte dans le Lasso.
    year_max : int
        Année jusqu'à laquelle faire tourner le programme
    alpha : float
        Paramètre régularisateur de la régression Lasso, plus le coefficient
        est faible plus on aura de variables retenues

    Returns
    -------
    results_allyears_ranked : pd.DataFrame
        Tableau trié donnant les meilleures variables prédictrices sur toutes
        les années

    """

    year = 12 #Pour année 2012
    results_allyears = find_features(type_snds, cat_target, path_snds, 
                                     path_affine, var_restricted, year, alpha)
    results_allyears.columns = ['Var', 'Factor_2012']
    
    for myyear in range(13, year_max + 1): # Pour début année 2013
        results_temp = find_features(type_snds, cat_target, path_snds, 
                                     path_affine, var_restricted, 
                                     myyear, alpha)
        results_temp.columns = ['Var', 'Factor_' + str(myyear)]
        results_allyears = pd.merge(left = results_allyears, 
                                    right = results_temp, 
                                    on = "Var", 
                                    how = "outer")
    
    results_allyears_ranked = summarize_rank_years(results_allyears).head(30)
    
    
    path_lasso = get_path_lasso(path_affine, cat_target, type_snds)
    results_allyears_ranked.to_csv(path_lasso, index = False)

    return results_allyears_ranked

def find_features_allyears_wotoppredictors(type_snds: str, 
                  cat_target: str, 
                  path_snds: str, 
                  path_affine: str, 
                  var_restricted: list[str], 
                  year_max: int, 
                  alpha: float) -> pd.DataFrame:
    """
    Donne les meilleures variables prédictrices du phénotype étudie, en 
    utilisant toutes les années jusqu'à year_max et en les triant. En enlevant
    les WOTP.

    Parameters
    ----------
    type_snds : str
        "Type" de SNDS étudié, choix "DCIR", "MCO" ou "SSR" pour le moment.
    cat_target : str
        Phénotype étudié (VHB chronique, VHC chronique, cirrhose, etc)
    path_snds : str
        Chemin du SNDS local pour le travail à l'IPLESP.
    path_affine : str
        Nom de la table affiné. 
    var_restricted : list[str]
        Optionnel : liste pour importer un nombre limité de variable qui seront 
        binarisées puis prises en compte dans le Lasso.
    year_max : int
        Année jusqu'à laquelle faire tourner le programme
    alpha : float
        Paramètre régularisateur de la régression Lasso, plus le coefficient
        est faible plus on aura de variables retenues

    Returns
    -------
    results : pd.DataFrame
        Tableau trié donnant les meilleures variables prédictrices

    """
    
    year = 12 #Pour année 2012
    results_allyears = find_features_wotoppredictors(type_snds, cat_target, 
                                                     path_snds, path_affine, 
                                                     var_restricted, year, 
                                                     alpha)
    results_allyears.columns = ['Var', 'Factor_2012']
    
    for myyear in range(13, year_max + 1): # Pour début année 2013
        results_temp = find_features_wotoppredictors(type_snds, cat_target, 
                                                     path_snds, path_affine, 
                                                     var_restricted, myyear, 
                                                     alpha)
        results_temp.columns = ['Var', 'Factor_' + str(myyear)]
        results_allyears = pd.merge(left = results_allyears, 
                                    right = results_temp, 
                                    on = "Var", 
                                    how = "outer")
    
    results_allyears_ranked = summarize_rank_years(results_allyears).head(30)
    
    path_lasso = get_path_lasso(path_affine, 
                                cat_target, 
                                type_snds, 
                                wotp = "wotp_")
    results_allyears_ranked.to_csv(path_lasso, index = False)
    
    
    return results_allyears_ranked



# =============================================================================
# Fonctions spécifiques les ALD issues de IR_IMB_R
# =============================================================================


def import_ald():
    """
    Importe la table IR_IMB_R en l'état brut
    
    No parameters
    -------------
    On pourrait mettre un "path_main" toutefois qui est hardcodé pour le moment

    Returns
    -------
    df_snds : pd.DataFrame()
        DataFrame de IR_IMB_R.
    var_na : list[str]
        Liste des variables manquantes.

    """
    path_main = "D:/Data/HEPAT_VUE_30012023/HEP_CSV/IR_IMB_R_202212.csv"
    df_snds_raw = pd.read_csv(path_main,
                              sep = ";")
    df_snds = df_snds_raw.drop_duplicates().dropna(axis = 1)
    var_na = list(set(df_snds_raw) - set(df_snds))
    
    return df_snds, var_na

def yearisation_ald(df_snds: pd.DataFrame, 
                    myyear: int) -> pd.DataFrame:
    """
    Donne l'indicatrice si le patient a une ALD pour l'année myyear d'intérêt

    Parameters
    ----------
    df_snds : pd.DataFrame
        IR_IMB_R en entier.
    myyear : int
        DESCRIPTION.

    Returns
    -------
    df_snds : pd.DataFrame()
        DataFrame de IR_IMB_R mais uniquement avec des ALD qui ont cours
        sur l'année étudiée

    """
    df_snds["IMB_ALD_DTD_time"] = df_snds["IMB_ALD_DTD"].apply(lambda x: datetime.strptime(x, "%d/%m/%Y").year)
    df_snds["IMB_ALD_DTF_time"] = df_snds["IMB_ALD_DTF"].apply(lambda x: datetime.strptime(x, "%d/%m/%Y").year)
    df_snds["yearized"] = np.where(
(df_snds["IMB_ALD_DTD_time"] <= myyear) & (df_snds["IMB_ALD_DTF_time"] >= myyear), 1, 0)
    
    df_snds = df_snds[df_snds["yearized"] == 1]
    
    return df_snds

def find_features_ald(cat_target: str, 
                      var_restricted: list[str], 
                      year: int, 
                      alpha: float) -> pd.DataFrame:
    """
    Combien les fonctions pour trouver les ALD binariées les plus pertinentes
    
    Parameters
    ----------
    cat_target : str
        DESCRIPTION.
    var_restricted : list[str]
        Liste des variables sur lesquelles on restreint l'analyse, ici moins de
        surprise que pour les autres tables du SNDS : uniquement MED_MTF_COD
    year : int
        Année étudiée.
    alpha : float
        Paramètre de configuration de la régression Lasso.

    Returns
    -------
    results : pd.DataFrame
        DESCRIPTION.

    """
    df_snds, var_na = import_ald()
    df_snds = yearisation_ald(df_snds, year).copy()
    df_snds = df_snds[["NUM_ENQ"] + var_restricted]
    flat_df = flatten_snds(df_snds, var_restricted)
    data = merge_status(flat_df, cat_target)
    data = resample_data(data.drop("NUM_ENQ", axis = 1), cat_target)
    results = top_predictors(alpha, data, cat_target)

    return results

def find_features_allyears_ald(cat_target: str, 
                               var_restricted: list[str], 
                               year_max: int, 
                               alpha: float) -> pd.DataFrame:
    """
    Parameters
    ----------
    cat_target : str
        DESCRIPTION.
    var_restricted : list[str]
        Liste des variables sur lesquelles on restreint l'analyse, ici moins de
        surprise que pour les autres tables du SNDS : uniquement MED_MTF_COD
    year_max : int
        Années étudiées jusqu'à year max.
    alpha : float
        Paramètre de configuration de la régression Lasso.

    Returns
    -------
    results : pd.DataFrame
        DESCRIPTION.

    """
    
    year = 2012 # Pour année 2012
    results_allyears = find_features_ald(cat_target, 
                                         var_restricted, 
                                         year, 
                                         alpha)
    results_allyears.columns = ['Var', 'Factor_2012']
    
    for myyear in range(2013, year_max + 1): # Pour début année 2013
        results_temp = find_features_ald(cat_target,
                                         var_restricted,
                                         year, 
                                         alpha)
        results_temp.columns = ['Var', 'Factor_' + str(myyear)]
        results_allyears = pd.merge(left = results_allyears, 
                                    right = results_temp, 
                                    on = "Var", 
                                    how = "outer")
    
    results_allyears_ranked = summarize_rank_years(results_allyears).head(30)
    
    path_to_save = "results/" + cat_target +"/lasso/lasso_IR_IMB_R.csv"
    results_allyears_ranked.to_csv(path_to_save, index = False)
    
    return results_allyears_ranked    