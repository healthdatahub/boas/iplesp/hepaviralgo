# -*- coding: utf-8 -*-
"""
Created on Thu May  4 09:20:00 2023

@author: Titouan Blaize - Health Data Hub

Simple fichier pour conserver des valeurs globales à travers les fichiers
"""
# Variables de jointure pour les tables du SNDS
dic_join_vars = {
    "DCIR" : ["FLX_DIS_DTD", 
              "FLX_TRT_DTD", 
              "FLX_EMT_TYP", 
              "FLX_EMT_NUM", 
              "FLX_EMT_ORD", 
              "ORG_CLE_NUM", 
              "DCT_ORD_NUM", 
              "PRS_ORD_NUM", 
              "REM_TYP_AFF"],
    
    "MCO"  : ["ETA_NUM", 
              "RSA_NUM"],
    
    "SSR"  : ["ETA_NUM", 
              "RHA_NUM"],
    
    "SSR_affine"  : ["ETA_NUM", 
                     "RHA_NUM",
                     "RHS_NUM"]
    }

# Variables retour du PMSI-MCO servant à filtrer les observations
var_retour_pmsimco = ['COH_NAI_RET', 
                      'COH_SEX_RET', 
                      'DAT_RET', 
                      'FHO_RET', 
                      'HOS_NNE_RET', 
                      'HOS_ORG_RET', 
                      'NAI_RET', 
                      'NIR_RET', 
                      'PMS_RET',
                      'SEJ_MER_RET',
                      'SEJ_RET', 
                      'SEX_RET']


# Liste de variables choisies à la main pour chronique B, post-Lasso 
# + comparaison avec papiers de Laurent Lam 2023
chronique_b_curated_old = {
    "ER_PRS_F" : ["PRS_NAT_REF_3314", 
                  "PRS_NAT_REF_3315", 
                  "PRS_NAT_REF_3316", 
                  "PRS_NAT_REF_3317"],
    
    "ER_ARO_F" : ["ARO_PRS_NAT_3211", 
                  "ARO_PRS_NAT_3316"],
    
    "ER_BIO_F" : ["BIO_PRS_IDE_126", 
                  "BIO_PRS_IDE_127", 
                  "BIO_PRS_IDE_320", 
                  "BIO_PRS_IDE_322", 
                  "BIO_PRS_IDE_351", 
                  "BIO_PRS_IDE_353", 
                  "BIO_PRS_IDE_354",
                  "BIO_PRS_IDE_514", 
                  "BIO_PRS_IDE_519",  
                  "BIO_PRS_IDE_522", 
                  "BIO_PRS_IDE_532", 
                  "BIO_PRS_IDE_552", 
                  "BIO_PRS_IDE_591", 
                  "BIO_PRS_IDE_592", 
                  "BIO_PRS_IDE_593",
                  "BIO_PRS_IDE_1015",
                  "BIO_PRS_IDE_1104", 
                  "BIO_PRS_IDE_1106",  
                  "BIO_PRS_IDE_1127", 
                  "BIO_PRS_IDE_1377",
                  "BIO_PRS_IDE_1577",
                  "BIO_PRS_IDE_1601", 
                  "BIO_PRS_IDE_1609",
                  "BIO_PRS_IDE_1740",
                  "BIO_PRS_IDE_1804", 
                  "BIO_PRS_IDE_1806",
                  "BIO_PRS_IDE_4120", 
                  "BIO_PRS_IDE_4124", 
                  "BIO_PRS_IDE_4125", 
                  "BIO_PRS_IDE_4500",
                  "BIO_PRS_IDE_4711",
                  "BIO_PRS_IDE_7318", 
                  "BIO_PRS_IDE_7402"],
    
    "ER_CAM_F" : ["CAM_PRS_IDE_HEQE002", 
                  "CAM_PRS_IDE_HLQM001", 
                  "CAM_PRS_IDE_HLQM002",  
                  "CAM_PRS_IDE_ZCQH001", 
                  "CAM_PRS_IDE_ZCQJ004", 
                  "CAM_PRS_IDE_ZCQM001", 
                  "CAM_PRS_IDE_ZCQM004",
                  "CAM_PRS_IDE_ZCQM005", 
                  "CAM_PRS_IDE_ZCQM006", 
                  "CAM_PRS_IDE_ZCQM008", 
                  "CAM_PRS_IDE_ZCQM010"],
    
    "ER_PHA_F" : ["PHA_PRS_C13_V08AB", 
                  "PHA_PRS_C13_C07AA", 
                  "PHA_PRS_C13_J05AF", 
                  "PHA_PRS_C13_J07BB", 
                  "PHA_PRS_C13_J05AR"],
    
    "ER_UCD_F" : ["UCD_UCD_COD_0000009405081", 
                  "UCD_UCD_COD_0000009398211",
                  "UCD_UCD_COD_0000009405750", 
                  "UCD_UCD_COD_0000009373671", 
                  "UCD_UCD_COD_0000009419806", 
                  "UCD_UCD_COD_0000009288310", 
                  "UCD_UCD_COD_0000009362940", 
                  "UCD_UCD_COD_0000009233208", 
                  "UCD_UCD_COD_0000009418008", 
                  "UCD_UCD_COD_0000009426195", 
                  "UCD_UCD_COD_0000009236247", 
                  "UCD_UCD_COD_0000009367587", 
                  "UCD_UCD_COD_0000009429414"],
    
    "ALD"      : ["MED_MTF_COD_B180", 
                  "MED_MTF_COD_B181", 
                  "MED_MTF_COD_B182", 
                  "MED_MTF_COD_B188", 
                  "MED_MTF_COD_C22"],
    
    "D"        : ["ASS_DGN_B180", 
                  "ASS_DGN_B181", 
                  "ASS_DGN_B182", 
                  "ASS_DGN_B169", 
                  "ASS_DGN_O984", 
                  "ASS_DGN_C220", 
                  "ASS_DGN_K703", 
                  "ASS_DGN_K746", 
                  "ASS_DGN_Z944", 
                  "ASS_DGN_Z512"],
    
    "A"        : ["CDC_ACT_EDLF017", 
                  "CDC_ACT_ELQH001", 
                  "CDC_ACT_HLQX013", 
                  "CDC_ACT_HLHJ003", 
                  "CDC_ACT_HEQE002", 
                  "CDC_ACT_HLQM001", 
                  "CDC_ACT_HLQM002", 
                  "CDC_ACT_ZCQH001", 
                  "CDC_ACT_ZCQJ004", 
                  "CDC_ACT_ZCQM001", 
                  "CDC_ACT_ZCQM004", 
                  "CDC_ACT_ZCQM005",
                  "CDC_ACT_ZCQM006",
                  "CDC_ACT_ZCQM008", 
                  "CDC_ACT_ZCQM010"],
    
    "B"        : ["GRG_GHM_14Z14", 
                  "GRG_GHM_06K02", 
                  "GRG_GHM_07K06", 
                  "GRG_GHM_07M02", 
                  "GRG_GHM_07M04", 
                  "GRG_GHM_07M06", 
                  "GRG_GHM_07M07", 
                  "GRG_GHM_07M08", 
                  "GRG_GHM_07M09", 
                  "GRG_GHM_07M10", 
                  "GRG_GHM_07M11", 
                  "GRG_GHM_07M12", 
                  "GRG_GHM_07M13", 
                  "GRG_GHM_07M14", 
                  "GRG_GHM_07M15"],
    
    "FL"       : ["NABM_COD_00004120", 
                  "NABM_COD_00004124"],
    
    "UM"       : ["DGN_PAL_C220", 
                  "DGN_REL_C220", 
                  "DGN_PAL_B181", 
                  "DGN_REL_B181", 
                  "DGN_PAL_B182", 
                  "DGN_REL_B182", 
                  "DGN_PAL_Z511", 
                  "DGN_PAL_Z944", 
                  "DGN_REL_Z944"],
    
    "FM"       : ["CCAM_COD_HEQE002", 
                  "CCAM_COD_ELQH001",
                  "CCAM_COD_HLQX013", 
                  "CCAM_COD_HLHJ003", 
                  "CCAM_COD_HLQM001", 
                  "CCAM_COD_HLQM002",  
                  "CCAM_COD_ZCQH001", 
                  "CCAM_COD_ZCQJ004", 
                  "CCAM_COD_ZCQM001", 
                  "CCAM_COD_ZCQM004", 
                  "CCAM_COD_ZCQM005", 
                  "CCAM_COD_ZCQM006", 
                  "CCAM_COD_ZCQM008", 
                  "CCAM_COD_ZCQM010"]
    }

chronique_b_curated = {
    "DCIR" : {
        "ER_PRS_F" : ["PRS_NAT_REF_3314", 
                      "PRS_NAT_REF_3315", 
                      "PRS_NAT_REF_3316", 
                      "PRS_NAT_REF_3317"],
        
        "ER_ARO_F" : ["ARO_PRS_NAT_3211", 
                      "ARO_PRS_NAT_3316"],
        
        "ER_BIO_F" : ["BIO_PRS_IDE_126", 
                      "BIO_PRS_IDE_127", 
                      "BIO_PRS_IDE_320", 
                      "BIO_PRS_IDE_322", 
                      "BIO_PRS_IDE_351", 
                      "BIO_PRS_IDE_353", 
                      "BIO_PRS_IDE_354",
                      "BIO_PRS_IDE_514", 
                      "BIO_PRS_IDE_519",  
                      "BIO_PRS_IDE_522", 
                      "BIO_PRS_IDE_532", 
                      "BIO_PRS_IDE_552", 
                      "BIO_PRS_IDE_591", 
                      "BIO_PRS_IDE_592", 
                      "BIO_PRS_IDE_593",
                      "BIO_PRS_IDE_1015",
                      "BIO_PRS_IDE_1104", 
                      "BIO_PRS_IDE_1106",  
                      "BIO_PRS_IDE_1127", 
                      "BIO_PRS_IDE_1377",
                      "BIO_PRS_IDE_1577",
                      "BIO_PRS_IDE_1601", 
                      "BIO_PRS_IDE_1609",
                      "BIO_PRS_IDE_1740",
                      "BIO_PRS_IDE_1804", 
                      "BIO_PRS_IDE_1806",
                      "BIO_PRS_IDE_4120", 
                      "BIO_PRS_IDE_4124", 
                      "BIO_PRS_IDE_4125", 
                      "BIO_PRS_IDE_4500",
                      "BIO_PRS_IDE_4711",
                      "BIO_PRS_IDE_7318", 
                      "BIO_PRS_IDE_7402"],
        
        "ER_CAM_F" : ["CAM_PRS_IDE_HEQE002", 
                      "CAM_PRS_IDE_HLQM001", 
                      "CAM_PRS_IDE_HLQM002",  
                      "CAM_PRS_IDE_ZCQH001", 
                      "CAM_PRS_IDE_ZCQJ004", 
                      "CAM_PRS_IDE_ZCQM001", 
                      "CAM_PRS_IDE_ZCQM004",
                      "CAM_PRS_IDE_ZCQM005", 
                      "CAM_PRS_IDE_ZCQM006", 
                      "CAM_PRS_IDE_ZCQM008", 
                      "CAM_PRS_IDE_ZCQM010"],
        
        "ER_PHA_F" : ["PHA_PRS_C13_V08AB", 
                      "PHA_PRS_C13_C07AA", 
                      "PHA_PRS_C13_J05AF", 
                      "PHA_PRS_C13_J07BB", 
                      "PHA_PRS_C13_J05AR"],
        
        "ER_UCD_F" : ["UCD_UCD_COD_0000009405081", 
                      "UCD_UCD_COD_0000009398211",
                      "UCD_UCD_COD_0000009405750", 
                      "UCD_UCD_COD_0000009373671", 
                      "UCD_UCD_COD_0000009419806", 
                      "UCD_UCD_COD_0000009288310", 
                      "UCD_UCD_COD_0000009362940", 
                      "UCD_UCD_COD_0000009233208", 
                      "UCD_UCD_COD_0000009418008", 
                      "UCD_UCD_COD_0000009426195", 
                      "UCD_UCD_COD_0000009236247", 
                      "UCD_UCD_COD_0000009367587", 
                      "UCD_UCD_COD_0000009429414"]
        },
    "MCO" : {
        "D"        : ["ASS_DGN_B180", 
                      "ASS_DGN_B181", 
                      "ASS_DGN_B182", 
                      "ASS_DGN_B169", 
                      "ASS_DGN_O984", 
                      "ASS_DGN_C220", 
                      "ASS_DGN_K703", 
                      "ASS_DGN_K746", 
                      "ASS_DGN_Z944", 
                      "ASS_DGN_Z512"],
        
        "A"        : ["CDC_ACT_EDLF017", 
                      "CDC_ACT_ELQH001", 
                      "CDC_ACT_HLQX013", 
                      "CDC_ACT_HLHJ003", 
                      "CDC_ACT_HEQE002", 
                      "CDC_ACT_HLQM001", 
                      "CDC_ACT_HLQM002", 
                      "CDC_ACT_ZCQH001", 
                      "CDC_ACT_ZCQJ004", 
                      "CDC_ACT_ZCQM001", 
                      "CDC_ACT_ZCQM004", 
                      "CDC_ACT_ZCQM005",
                      "CDC_ACT_ZCQM006",
                      "CDC_ACT_ZCQM008", 
                      "CDC_ACT_ZCQM010"],
        
        "B"        : ["GRG_GHM_14Z14", 
                      "GRG_GHM_06K02", 
                      "GRG_GHM_07K06", 
                      "GRG_GHM_07M02", 
                      "GRG_GHM_07M04", 
                      "GRG_GHM_07M06", 
                      "GRG_GHM_07M07", 
                      "GRG_GHM_07M08", 
                      "GRG_GHM_07M09", 
                      "GRG_GHM_07M10", 
                      "GRG_GHM_07M11", 
                      "GRG_GHM_07M12", 
                      "GRG_GHM_07M13", 
                      "GRG_GHM_07M14", 
                      "GRG_GHM_07M15"],
        
        "FL"       : ["NABM_COD_00004120", 
                      "NABM_COD_00004124"],
        
        "UM"       : ["DGN_PAL_C220", 
                      "DGN_REL_C220", 
                      "DGN_PAL_B181", 
                      "DGN_REL_B181", 
                      "DGN_PAL_B182", 
                      "DGN_REL_B182", 
                      "DGN_PAL_Z511", 
                      "DGN_PAL_Z944", 
                      "DGN_REL_Z944"],
        
        "FM"       : ["CCAM_COD_HEQE002", 
                      "CCAM_COD_ELQH001",
                      "CCAM_COD_HLQX013", 
                      "CCAM_COD_HLHJ003", 
                      "CCAM_COD_HLQM001", 
                      "CCAM_COD_HLQM002",  
                      "CCAM_COD_ZCQH001", 
                      "CCAM_COD_ZCQJ004", 
                      "CCAM_COD_ZCQM001", 
                      "CCAM_COD_ZCQM004", 
                      "CCAM_COD_ZCQM005", 
                      "CCAM_COD_ZCQM006", 
                      "CCAM_COD_ZCQM008", 
                      "CCAM_COD_ZCQM010"]
        },
    "SSR" : {

        
        },
    
    "ALD"      : ["MED_MTF_COD_B180", 
                  "MED_MTF_COD_B181", 
                  "MED_MTF_COD_B182", 
                  "MED_MTF_COD_B188", 
                  "MED_MTF_COD_C22"]
    }
    


chronique_c_curated = {
    "DCIR" : {
        "ER_PRS_F" : ["PRS_NAT_REF_3315",
                      "PRS_NAT_REF_3317"],
        
        "ER_BIO_F" : ["BIO_PRS_IDE_320", 
                      "BIO_PRS_IDE_322", 
                      "BIO_PRS_IDE_323",
                      "BIO_PRS_IDE_351", 
                      "BIO_PRS_IDE_353", 
                      "BIO_PRS_IDE_354",  
                      "BIO_PRS_IDE_522", 
                      "BIO_PRS_IDE_578", 
                      "BIO_PRS_IDE_1100", 
                      "BIO_PRS_IDE_1208",
                      "BIO_PRS_IDE_1740",
                      "BIO_PRS_IDE_1806",
                      "BIO_PRS_IDE_4120", 
                      "BIO_PRS_IDE_4124", 
                      "BIO_PRS_IDE_4125", 
                      "BIO_PRS_IDE_4500",
                      "BIO_PRS_IDE_4711"],
        
        "ER_CAM_F" : ["CAM_PRS_IDE_HEQE002", 
                      "CAM_PRS_IDE_HLQM001", 
                      "CAM_PRS_IDE_HLQM002",  
                      "CAM_PRS_IDE_ZCQH001", 
                      "CAM_PRS_IDE_ZCQJ004", 
                      "CAM_PRS_IDE_ZCQM001", 
                      "CAM_PRS_IDE_ZCQM004",
                      "CAM_PRS_IDE_ZCQM005", 
                      "CAM_PRS_IDE_ZCQM006", 
                      "CAM_PRS_IDE_ZCQM008", 
                      "CAM_PRS_IDE_ZCQM010"],
        
        "ER_PHA_F" : ["PHA_PRS_C13_V08AB", 
                      "PHA_PRS_C13_C07AA", 
                      "PHA_PRS_C13_J05AF", 
                      "PHA_PRS_C13_J05AB",
                      "PHA_PRS_C13_J07BB", 
                      "PHA_PRS_C13_J05AR"],
        
        "ER_UCD_F" : ["UCD_UCD_COD_J05AB", 
                      "UCD_UCD_COD_J05AF",
                      "UCD_UCD_COD_J05AP", 
                      "UCD_UCD_COD_J05AR", 
                      "UCD_UCD_COD_J05AX", 
                      "UCD_UCD_COD_J05AE"]
        },
    "MCO" : {
        "D"        : ["ASS_DGN_B180", 
                      "ASS_DGN_B181", 
                      "ASS_DGN_B182", 
                      "ASS_DGN_B169", 
                      "ASS_DGN_O984", 
                      "ASS_DGN_C220", 
                      "ASS_DGN_K703",
                      "ASS_DGN_Z720",
                      "ASS_DGN_K746", 
                      "ASS_DGN_Z944", 
                      "ASS_DGN_Z512"],
        
        "A"        : ["CDC_ACT_EDLF017", 
                      "CDC_ACT_ELQH001", 
                      "CDC_ACT_HLQX013", 
                      "CDC_ACT_HLHJ003", 
                      "CDC_ACT_HEQE002", 
                      "CDC_ACT_HLQM001", 
                      "CDC_ACT_HLQM002", 
                      "CDC_ACT_ZCQH001", 
                      "CDC_ACT_ZCQJ004", 
                      "CDC_ACT_ZCQM001", 
                      "CDC_ACT_ZCQM004", 
                      "CDC_ACT_ZCQM005",
                      "CDC_ACT_ZCQM006",
                      "CDC_ACT_ZCQM008", 
                      "CDC_ACT_ZCQM010"],
        
        "B"        : ["GRG_GHM_14Z14", 
                      "GRG_GHM_06K02", 
                      "GRG_GHM_07K06", 
                      "GRG_GHM_07M02", 
                      "GRG_GHM_07M04", 
                      "GRG_GHM_07M06", 
                      "GRG_GHM_07M07", 
                      "GRG_GHM_07M08", 
                      "GRG_GHM_07M09", 
                      "GRG_GHM_07M10", 
                      "GRG_GHM_07M11", 
                      "GRG_GHM_07M12", 
                      "GRG_GHM_07M13", 
                      "GRG_GHM_07M14", 
                      "GRG_GHM_07M15"],
        
        "FL"       : ["NABM_COD_00004120", 
                      "NABM_COD_00004124"],
        
        "UM"       : ["DGN_PAL_C220", 
                      "DGN_REL_C220", 
                      "DGN_PAL_B181", 
                      "DGN_REL_B181", 
                      "DGN_PAL_B182", 
                      "DGN_REL_B182", 
                      "DGN_PAL_Z511", 
                      "DGN_PAL_Z944", 
                      "DGN_REL_Z944"],
        
        "FM"       : ["CCAM_COD_HEQE002", 
                      "CCAM_COD_ELQH001",
                      "CCAM_COD_HLQX013", 
                      "CCAM_COD_HLHJ003", 
                      "CCAM_COD_HLQM001", 
                      "CCAM_COD_HLQM002",  
                      "CCAM_COD_ZCQH001", 
                      "CCAM_COD_ZCQJ004", 
                      "CCAM_COD_ZCQM001", 
                      "CCAM_COD_ZCQM004", 
                      "CCAM_COD_ZCQM005", 
                      "CCAM_COD_ZCQM006", 
                      "CCAM_COD_ZCQM008", 
                      "CCAM_COD_ZCQM010"]
        },
    "SSR" : {
        "B"        : ["FP_PEC_Z502"],
        
        "D"        : ["DGN_COD_B181", 
                      "DGN_COD_B182", 
                      "DGN_COD_K746",
                      "DGN_COD_Z944"]
        },
    
    "ALD"      : ["MED_MTF_COD_B180", 
                  "MED_MTF_COD_B181", 
                  "MED_MTF_COD_B182", 
                  "MED_MTF_COD_B188", 
                  "MED_MTF_COD_C22",
                  "MED_MTF_COD_K74",
                  "MED_MTF_COD_Z94"]
    }
    

# Liste de variables choisies à la main pour firbose F4 , post-Lasso 
# + discussion avec Laurent Lam

fibrose_f4_b_curated = {
    "DCIR" : {
        "ER_PRS_F" : ["PSE_SPE_COD_8", 
                      "PSP_SPE_COD_8", 
                      "PRS_NAT_REF_3211"],
        
        "ER_BIO_F" : ["BIO_PRS_IDE_126", 
                      "BIO_PRS_IDE_127", 
                      "BIO_PRS_IDE_320",
                      "BIO_PRS_IDE_514",  
                      "BIO_PRS_IDE_519", 
                      "BIO_PRS_IDE_522", 
                      "BIO_PRS_IDE_592", 
                      "BIO_PRS_IDE_593",
                      "BIO_PRS_IDE_1000",
                      "BIO_PRS_IDE_1015",
                      "BIO_PRS_IDE_1104", 
                      "BIO_PRS_IDE_1106",  
                      "BIO_PRS_IDE_1127", 
                      "BIO_PRS_IDE_1377",
                      "BIO_PRS_IDE_1601", 
                      "BIO_PRS_IDE_1609",
                      "BIO_PRS_IDE_1804", 
                      "BIO_PRS_IDE_1806",
                      "BIO_PRS_IDE_4120", 
                      "BIO_PRS_IDE_7318",
                      "BIO_PRS_IDE_7402"],
        
        "ER_CAM_F" : ["CAM_PRS_IDE_HEQE002", 
                      "CAM_PRS_IDE_HLQM001", 
                      "CAM_PRS_IDE_HLQM002", 
                      "CAM_PRS_IDE_ZCQH001", 
                      "CAM_PRS_IDE_ZCQJ004", 
                      "CAM_PRS_IDE_ZCQM001", 
                      "CAM_PRS_IDE_ZCQM004", 
                      "CAM_PRS_IDE_ZCQM005", 
                      "CAM_PRS_IDE_ZCQM006", 
                      "CAM_PRS_IDE_ZCQM008", 
                      "CAM_PRS_IDE_ZCQM010"],
        
        "ER_PHA_F" : ["PHA_PRS_C13_V08AB", 
                      "PHA_PRS_C13_C07AA", 
                      "PHA_PRS_C13_J05AF", 
                      "PHA_PRS_C13_J07BB", 
                      "PHA_PRS_C13_J05AR"],
        
        "ER_UCD_F" : ["UCD_UCD_COD_J05AB", 
                      "UCD_UCD_COD_J05AP",
                      "UCD_UCD_COD_J05AX",
                      "UCD_UCD_COD_J05AR",
                      "UCD_UCD_COD_J05AF"
                      ]
        },
    "MCO" : {
        "D"        : ["ASS_DGN_B180", 
                      "ASS_DGN_C220", 
                      "ASS_DGN_K703", 
                      "ASS_DGN_K746", 
                      "ASS_DGN_Z944", 
                      "ASS_DGN_Z512", 
                      "ASS_DGN_R18", 
                      "ASS_DGN_K766"],
        
        "A"        : ["CDC_ACT_HEQE002", 
                      "CDC_ACT_HLQM001", 
                      "CDC_ACT_HLQM002", 
                      "CDC_ACT_ZCQH001", 
                      "CDC_ACT_ZCQJ004", 
                      "CDC_ACT_ZCQM001", 
                      "CDC_ACT_ZCQM004", 
                      "CDC_ACT_ZCQM005", 
                      "CDC_ACT_ZCQM006", 
                      "CDC_ACT_ZCQM008", 
                      "CDC_ACT_ZCQM010"],
        
        "B"        : ["GRG_GHM_07M02", 
                      "GRG_GHM_07M04", 
                      "GRG_GHM_07M06", 
                      "GRG_GHM_07M07", 
                      "GRG_GHM_07M08", 
                      "GRG_GHM_07M09", 
                      "GRG_GHM_07M10", 
                      "GRG_GHM_07M11", 
                      "GRG_GHM_07M12", 
                      "GRG_GHM_07M13", 
                      "GRG_GHM_07M14", 
                      "GRG_GHM_07M15"],

        
        "UM"       : ["DGN_PAL_C220", 
                      "DGN_REL_C220", 
                      "DGN_PAL_K746", 
                      "DGN_REL_K746",
                      "DGN_PAL_Z098", 
                      "DGN_PAL_Z088", 
                      "DGN_PAL_Z511", 
                      "DGN_PAL_Z092",
                      "DGN_PAL_Z944", 
                      "DGN_REL_Z944", 
                      "DGN_PAL_I859", 
                      "DGN_PAL_I982", 
                      "DGN_PAL_R18", 
                      "DGN_PAL_C229"],
        
        "FM"       : ["CCAM_COD_HEQE002"]
        },
    
    "SSR" : {
        "B"        : ["MOR_PRP_Z967"],
        
        "D"        : ["DGN_COD_B181", 
                      "DGN_COD_B182", 
                      "DGN_COD_K746",
                      "DGN_COD_Z944"]
        },
    
    "ALD"      : ["MED_MTF_COD_K74", 
                  "MED_MTF_COD_C22", 
                  "MED_MTF_COD_K703",
                  "MED_MTF_COD_F60",
                  "MED_MTF_COD_Z94"],
    }



fibrose_f4_b_curated_old = {
    "ER_PRS_F" : ["PSE_SPE_COD_8", 
                  "PSP_SPE_COD_8", 
                  "PRS_NAT_REF_3211"],
    
    "ER_ARO_F" : ["ARO_PRS_NAT_3211", 
                  "ARO_PRS_NAT_1352", 
                  "ARO_PRS_NAT_1324"],
    
    "ER_BIO_F" : ["BIO_PRS_IDE_126", 
                  "BIO_PRS_IDE_127", 
                  "BIO_PRS_IDE_320",
                  "BIO_PRS_IDE_322", 
                  "BIO_PRS_IDE_351", 
                  "BIO_PRS_IDE_353", 
                  "BIO_PRS_IDE_354",
                  "BIO_PRS_IDE_514",  
                  "BIO_PRS_IDE_519", 
                  "BIO_PRS_IDE_522", 
                  "BIO_PRS_IDE_532", 
                  "BIO_PRS_IDE_552", 
                  "BIO_PRS_IDE_591", 
                  "BIO_PRS_IDE_592", 
                  "BIO_PRS_IDE_593",
                  "BIO_PRS_IDE_1015",
                  "BIO_PRS_IDE_1104", 
                  "BIO_PRS_IDE_1106",  
                  "BIO_PRS_IDE_1127", 
                  "BIO_PRS_IDE_1377",
                  "BIO_PRS_IDE_1465", 
                  "BIO_PRS_IDE_1466",
                  "BIO_PRS_IDE_1467", 
                  "BIO_PRS_IDE_1468", 
                  "BIO_PRS_IDE_1469", 
                  "BIO_PRS_IDE_1471", 
                  "BIO_PRS_IDE_1472", 
                  "BIO_PRS_IDE_1473",
                  "BIO_PRS_IDE_1577",
                  "BIO_PRS_IDE_1601", 
                  "BIO_PRS_IDE_1609",
                  "BIO_PRS_IDE_1804", 
                  "BIO_PRS_IDE_1806",
                  "BIO_PRS_IDE_4120", 
                  "BIO_PRS_IDE_7318"],
    
    "ER_CAM_F" : ["CAM_PRS_IDE_HEQE002", 
                  "CAM_PRS_IDE_ELQH001", 
                  "CAM_PRS_IDE_HLQX013",
                  "CAM_PRS_IDE_HLHJ003", 
                  "CAM_PRS_IDE_HLQM001", 
                  "CAM_PRS_IDE_HLQM002", 
                  "CAM_PRS_IDE_ZCQH001", 
                  "CAM_PRS_IDE_ZCQJ004", 
                  "CAM_PRS_IDE_ZCQM001", 
                  "CAM_PRS_IDE_ZCQM004", 
                  "CAM_PRS_IDE_ZCQM005", 
                  "CAM_PRS_IDE_ZCQM006", 
                  "CAM_PRS_IDE_ZCQM008", 
                  "CAM_PRS_IDE_ZCQM010"],
    
    "ER_PHA_F" : ["PHA_PRS_C13_V08AB", 
                  "PHA_PRS_C13_C07AA", 
                  "PHA_PRS_C13_J05AF", 
                  "PHA_PRS_C13_J07BB", 
                  "PHA_PRS_C13_J05AR"],
    
    "ER_UCD_F" : ["UCD_UCD_COD_0000009233208", 
                  "UCD_UCD_COD_0000009353059"],
    
    "ALD"      : ["MED_MTF_COD_K74", 
                  "MED_MTF_COD_C22", 
                  "MED_MTF_COD_K703"],
    
    "D"        : ["ASS_DGN_B180", 
                  "ASS_DGN_C220", 
                  "ASS_DGN_K703", 
                  "ASS_DGN_K746", 
                  "ASS_DGN_Z944", 
                  "ASS_DGN_Z512", 
                  "ASS_DGN_R18", 
                  "ASS_DGN_K766"],
    
    "A"        : ["CDC_ACT_EDLF017", 
                  "CDC_ACT_ELQH001",
                  "CDC_ACT_HLQX013",
                  "CDC_ACT_HLHJ003", 
                  "CDC_ACT_HEQE002", 
                  "CDC_ACT_HLQM001", 
                  "CDC_ACT_HLQM002", 
                  "CDC_ACT_ZCQH001", 
                  "CDC_ACT_ZCQJ004", 
                  "CDC_ACT_ZCQM001", 
                  "CDC_ACT_ZCQM004", 
                  "CDC_ACT_ZCQM005", 
                  "CDC_ACT_ZCQM006", 
                  "CDC_ACT_ZCQM008", 
                  "CDC_ACT_ZCQM010"],
    
    "B"        : ["GRG_GHM_07M02", 
                  "GRG_GHM_07M04", 
                  "GRG_GHM_07M06", 
                  "GRG_GHM_07M07", 
                  "GRG_GHM_07M08", 
                  "GRG_GHM_07M09", 
                  "GRG_GHM_07M10", 
                  "GRG_GHM_07M11", 
                  "GRG_GHM_07M12", 
                  "GRG_GHM_07M13", 
                  "GRG_GHM_07M14", 
                  "GRG_GHM_07M15"],
    
    "FL"       : ["NABM_COD_00007335", 
                  "NABM_COD_00000127", 
                  "NABM_COD_00000214"],
    
    "UM"       : ["DGN_PAL_C220", 
                  "DGN_REL_C220", 
                  "DGN_PAL_K746", 
                  "DGN_REL_K746",
                  "DGN_PAL_Z098", 
                  "DGN_PAL_Z088", 
                  "DGN_PAL_Z511", 
                  "DGN_PAL_Z092",
                  "DGN_PAL_Z944", 
                  "DGN_REL_Z944", 
                  "DGN_PAL_I859", 
                  "DGN_PAL_R18", 
                  "DGN_PAL_C229"],
    
    "FM"       : ["CCAM_COD_HEQE002", 
                  "CCAM_COD_ELQH001", 
                  "CCAM_COD_HLQX013", 
                  "CCAM_COD_HLHJ003", 
                  "CCAM_COD_HLQM001", 
                  "CCAM_COD_HLQM002",  
                  "CCAM_COD_ZCQH001", 
                  "CCAM_COD_ZCQJ004", 
                  "CCAM_COD_ZCQM001", 
                  "CCAM_COD_ZCQM004", 
                  "CCAM_COD_ZCQM005", 
                  "CCAM_COD_ZCQM006", 
                  "CCAM_COD_ZCQM008", 
                  "CCAM_COD_ZCQM010"]
    }


HEPAD_curated = {
    "ER_PRS_F" : ["PRS_NAT_REF_3316", 
                  "PSP_SPE_COD_8", 
                  "PRS_NAT_REF_3211"],
    
    "ER_BIO_F" : [
                  "BIO_PRS_IDE_323",
                  "BIO_PRS_IDE_514",  
                  "BIO_PRS_IDE_519", 
                  "BIO_PRS_IDE_521",
                  "BIO_PRS_IDE_522", 
                  "BIO_PRS_IDE_524", 
                  "BIO_PRS_IDE_591", 
                  "BIO_PRS_IDE_592", 
                  "BIO_PRS_IDE_996",
                  "BIO_PRS_IDE_1740",
                  "BIO_PRS_IDE_1741",
                  "BIO_PRS_IDE_1742",
                  "BIO_PRS_IDE_1806", 
                  "BIO_PRS_IDE_4118",
                  "BIO_PRS_IDE_4119",
                  "BIO_PRS_IDE_4120", 
                  "BIO_PRS_IDE_7318"],
    
    "ER_CAM_F" : ["CAM_PRS_IDE_HEQE002", 
                  "CAM_PRS_IDE_ELQH001", 
                  "CAM_PRS_IDE_HLQX013",
                  "CAM_PRS_IDE_HLHJ003", 
                  "CAM_PRS_IDE_HLQM001", 
                  "CAM_PRS_IDE_HLQM002", 
                  "CAM_PRS_IDE_ZCQH001", 
                  "CAM_PRS_IDE_ZCQJ004", 
                  "CAM_PRS_IDE_ZCQM001", 
                  "CAM_PRS_IDE_ZCQM004", 
                  "CAM_PRS_IDE_ZCQM005", 
                  "CAM_PRS_IDE_ZCQM006", 
                  "CAM_PRS_IDE_ZCQM008", 
                  "CAM_PRS_IDE_ZCQM010"],
    
    "ER_PHA_F" : ["PHA_PRS_C13_J05AX"],
    
    "ER_UCD_F" : ["UCD_UCD_COD_J05AX"],
    
    "ALD"      : ["MED_MTF_COD_B180", 
                  "MED_MTF_COD_B188"],
    
    "D"        : ["ASS_DGN_B180", 
                  "ASS_DGN_B188"],
    
    "B"        : ["GRG_GHM_07M02", 
              "GRG_GHM_07M04", 
              "GRG_GHM_07M06", 
              "GRG_GHM_07M07", 
              "GRG_GHM_07M08", 
              "GRG_GHM_07M09", 
              "GRG_GHM_07M10", 
              "GRG_GHM_07M11", 
              "GRG_GHM_07M12", 
              "GRG_GHM_07M13", 
              "GRG_GHM_07M14", 
              "GRG_GHM_07M15"],
    
    "UM"       : ["DGN_PAL_C220", 
              "DGN_REL_C220", 
              "DGN_PAL_K746", 
              "DGN_REL_K746",
              "DGN_PAL_Z098", 
              "DGN_PAL_Z088", 
              "DGN_PAL_Z511", 
              "DGN_PAL_Z092",
              "DGN_PAL_Z944", 
              "DGN_REL_Z944", 
              "DGN_PAL_I859", 
              "DGN_PAL_B180", 
              "DGN_REL_B180", 
              "DGN_PAL_B188", 
              "DGN_REL_B188", 
              "DGN_PAL_C229"],
    }