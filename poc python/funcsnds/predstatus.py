# -*- coding: utf-8 -*-
"""
Fonctions pour préparer réaliser le datamanagement du SNDS : récupérer les 
valeurs des nomenclatures de certains variables  au sein des tables, réaliser 
les merges associés, regrouper les informations au format "une ligne : un 
patient, colonnes : le nombre d'évènements d'un nomenclature donnée"
Fusionner avec les résultats d'Hépather pour avoir le gold standard
Créer les métriques pour évaluer le modèle
"""

# =============================================================================
# Packages utilisées
# =============================================================================


import pandas as pd
import funcsnds.featureselection as fs
import funcsnds.featurecleaning as fc
import funcsnds.globalvar as gv
from datetime import datetime


dic_join_vars = gv.dic_join_vars

# =============================================================================
# Fonctions outils utilisées par la suite
# =============================================================================


def merge_years(saved_files: list[pd.DataFrame],
                cat_target: str) -> pd.DataFrame:
    """
    Rassemble les scores annuels en une seule dataframe globale
    Parameters
    ----------
    saved_files : list[pd.DataFrame]
        Liste des résultats/scores annuels.
    cat_target : str
        Phénoytpe étuidéé.

    Returns
    -------
    df_total : pd.DataFrame
        Dataframe résumant les scores globaux.

    """
    
    # Initialisation de df_total avant de lancer la boucle
    df_total = pd.merge(left = saved_files[0], 
                        right = saved_files[1], 
                        on = ["NUM_ENQ", cat_target])
    for mycount in range(2, len(saved_files)):
        df_total = pd.merge(left = df_total, 
                            right = saved_files[mycount], 
                            on = ["NUM_ENQ", cat_target])
        
    return df_total

def last_split(mystr: str) -> str:
    """
    Permet de récupérer la valeur de la nomenclature associée à une variable
    Par exemple "BIO_PRS_IDE_4124" retourne "4124"

    Parameters
    ----------
    mystr : str
        "BIO_PRS_IDE_4124".

    Returns
    -------
    str
        "4124".

    """
    temp = mystr[::-1].split("/", 1)
    return [temp[1][::-1], temp[0][::-1]]



def var_from_results(var_to_keep: list[str]) -> list[str]:
    """
    A partir des X meilleures valeurs de nomenclatures (variables binarisées), 
    reconstitue les variables (non binarisées) dans lesquelles sont ces 
    occurences de nomenclatures
    

    Parameters
    ----------
    var_to_keep : list[str]
        Liste des variables binarisées issues du Lasso.

    Returns
    -------
    list[str]
        Liste des variables non binarisées natives du SNDS.

    """
    
    list_var = []
    for elem in var_to_keep:
        #list_var.append(elem.split("/")[0])
        list_var.append((last_split(elem)[0]))
    return list(set(list_var))

def extract_values_used(mycol, list_var):
    
    res = []
    for elem in list_var:
        if mycol in elem :
            #res.append(elem.split("/")[1])
            res.append((last_split(elem)[1]))
    return res
       
def rename_affine(mydf: pd.DataFrame, 
                  type_pmsi: str,
                  path_affine: str, 
                  joins_vars: list[str]) -> list[str]:
    """
    Renomme les variables des tables affinées pour savoir de quelles tables 
    sont issues les variables binarisées retenues
    Utilise le nom de la table comme préfixe

    Parameters
    ----------
    mydf : pd.DataFrame
        Dataframe d'origine.
    type_pmsi : str
        MCO ou SSR
    path_affine : str
        Table affinée utilisée.
    joins_vars : list[str]
        Variables de jointures.

    Returns
    -------
    list[str]
        Nouveaux noms de colonnes avec préfixe de la table affinée.

    """
    mycols = mydf.columns
    newcols = []
    for elem in mycols:
        if elem in joins_vars:
            newcols.append(elem)
        else:
            newcols.append(type_pmsi + "/" + path_affine + "/" + elem)
    return newcols
                    
def flatten_and_fill(df_total: pd.DataFrame,
                     vars_total: list[str], 
                     nomenc_total: list[str]) -> pd.DataFrame:
    """ 
    A partir d'une table issue et au format du SNDS, l'aplatit de manière à
    ne garder que les nomenclatures des variables pertinentes, binarisées pour
    utilisation par des algorithmes de machine learning

    Parameters
    ----------
    df_total : pd.DataFrame
        Table issue et au format du SNDS.
    vars_total : list[str]
        Variables à retenir.
    nomenc_total : list[str]
        DESCRIPTION.

    Returns
    -------
    df_flatten : pd.DataFrame
        Table du SNDS binariées et restreintes aux variables pertinentes 
        pour le ML.

    """
    
    df_flatten = pd.DataFrame(df_total["NUM_ENQ"].unique(), 
                              columns = ["NUM_ENQ"])
    for myvar in vars_total:
        values_used = extract_values_used(myvar, nomenc_total)
        # On remplace les valeurs de variables non pertinentes et les "fill" 
        # avec "OTHER_NOMENC".Ceci permet de binariser/pd.get_dummies 
        # beaucoup plus vite qu'une méthode "greedy"
        df_total.loc[~df_total[myvar].isin(values_used), myvar]= "OTHER_NOMENC"
        
        # INFORMATION IMPORTANTE : le .sum() à la fin du groupby somme les 
        # évènements, ce qui fait sens pour des prestations ou évènement - 
        # on peut faire plusieurs fois un certain évènement - mais pas pour 
        # des variables démographiques - on est pas plusieurs fois un homme ou 
        # résident d'un département
        flat_temp = df_total[["NUM_ENQ"]].join(pd.get_dummies(data = df_total[myvar],
                                                              prefix = myvar,
                                                              prefix_sep="/")).groupby("NUM_ENQ").sum()
        df_flatten = pd.merge(left = df_flatten, 
                              right = flat_temp, 
                              how = "inner", 
                              on = "NUM_ENQ")
        
    return df_flatten

# =============================================================================
# Data management du SNDS
# =============================================================================


def import_dcir(how_many_vars: int, 
                curated_vars: dict, 
                year: int, 
                cat_target: str, 
                path_affine: str, 
                path_snds: str):
    """ 
    Importe les tables relatives au DCIR, que ce soit ER_PRS_F ou les tables 
    affinées. Seules les nomenclatures de certaines variables retenues par la 
    première méthode lasso,  ou précisées dans curated_vars sont retenues afin 
    de limiter le temps d'importation

    Parameters
    ----------
    how_many_vars : int
        Combien de variables retenues par table depuis les résultats de Lasso,
        classées par ordre d'importance
    curated_vars : dict
        Si différent de None, outrepasse les résultats du Lasso pour fournir 
        soit-même une liste déterminée par analyse métier/à la main.
        Le format du dictionnaire attendu est donné dans globalvar
    year : int
        Année de la table importée
    cat_target : str
        Permet de déterminer les variables du Lasso correspondantes au
        phénotype étudié
    path_affine : str
        Chemin de la table affinée du DCIR, si path_affine = None alors on
        importe ER_PRS_F
    path_snds : str
        Chemin vers l'extraction du SNDS locale

    Returns
    -------
    df_dcir : pd.DataFrame
        Table importée finale
    vars_to_keep : list de str
        Variables retenues par la feature selection/analyse métier pour 
        importation.
    nomenc_to_keep : list de str
        Nomenclatures des variables retenues.

    """
    
    # Importe ER_PRS_F
    if (path_affine == None): 
        # Importe les meilleures variables issues du lasso
        if (curated_vars == None) : 
            path_lasso = "results/" + cat_target + "/lasso/lasso_ER_PRS_F.csv"
            import_lasso = pd.read_csv(path_lasso)
            nomenc_to_keep = list(import_lasso["Var"].head(how_many_vars))
        # Un dictionnaire des variables retenues pour ER_PRS_F est donné
        else : 
            nomenc_to_keep = curated_vars["DCIR"]["ER_PRS_F"].copy()
            
        vars_to_keep = var_from_results(nomenc_to_keep)
        file_er_prs_f = "ER_PRS_F_20" + str(year) + ".csv"
        cols_dcir = ["NUM_ENQ"] + vars_to_keep + ["CPL_MAJ_TOP", "DPN_QLF"] + dic_join_vars["DCIR"]
        df_dcir = pd.read_csv(path_snds + file_er_prs_f, 
                              sep = ";", 
                              usecols = cols_dcir, 
                              dtype = "object")
        df_dcir = fc.clean_erprsf(df_dcir).copy()
    
    # Importe une table affinée du DCIR
    else: 
        
        if (curated_vars == None) :
            
            path_lasso = "results/" + cat_target + "/lasso/lasso_" + path_affine + ".csv"
            import_lasso = pd.read_csv(path_lasso)
            nomenc_to_keep = list(import_lasso["Var"].head(how_many_vars))
            
            # Le lasso pouvant laisser de côté des variables correlées entre 
            # elles, un deuxième a été éventuellement lancé 
            # WOTP = WithOut Top Predictors
            try: 
                path_lasso = "results/" + cat_target + "/lasso/lasso_wotp_" + path_affine + ".csv"
                import_lasso_wotp = pd.read_csv(path_lasso)
                nomenc_to_keep_wotp = list(import_lasso_wotp["Var"].head(how_many_vars))
                nomenc_to_keep.extend(nomenc_to_keep_wotp)
            except:
                pass
        
        else :
            nomenc_to_keep = curated_vars["DCIR"][path_affine].copy()
            
        
        vars_to_keep = var_from_results(nomenc_to_keep)
        file_affine =  path_snds + path_affine + "_20" + str(year) + ".csv"
        if (path_affine == "ER_CAM_F"):
            df_dcir = pd.read_csv(file_affine, 
                                  sep = ";", 
                                  usecols = vars_to_keep + dic_join_vars["DCIR"], 
                                  dtype = "object", 
                                  encoding_errors="ignore")
        else:
            df_dcir = pd.read_csv(file_affine, 
                                  sep = ";", 
                                  usecols = vars_to_keep + dic_join_vars["DCIR"],
                                  dtype = "object")   
        df_dcir = fc.clean_dcir_affine(df_dcir).copy()
        
    return df_dcir, vars_to_keep, nomenc_to_keep

def import_mco(how_many_vars : int , 
               curated_vars : dict, 
               year: int,
               cat_target: str, 
               path_affine: str, 
               path_snds: str):
    
    """ 
    Importe les tables relatives au MCO, que ce soit T_MCOaaB ou les tables 
    affinées. Seules les nomenclatures de certaines variables retenues par la 
    première méthode lasso,  ou précisées dans curated_vars sont retenues afin 
    de limiter le temps d'importation

    Parameters
    ----------
    how_many_vars : int
        Combien de variables retenues par table depuis les résultats de Lasso,
        classées par ordre d'importance
    curated_vars : dict
        Si différent de None, outrepasse les résultats du Lasso pour fournir 
        soit-même une liste déterminée par analyse métier/à la main.
        Le format du dictionnaire attendu est donné dans globalvar
    year : int
        Année de la table importée
    cat_target : str
        Permet de déterminer les variables du Lasso correspondantes au
        phénotype étudié
    path_affine : str
        Chemin de la table affinée du PMSI_MCO, si path_affine = None alors on
        importe T_MCOaaB
    path_snds : str
        Chemin vers l'extraction du SNDS locale

    Returns
    -------
    df_mco : pd.DataFrame
        Table importée finale
    vars_to_keep : list de str
        Variables retenues par la feature selection/analyse métier pour 
        importation.
    nomenc_to_keep : list de str
        Nomenclatures des variables retenues.

    """
    
    if (path_affine == None):
        if (curated_vars == None) :
            
            path_lasso = "results/" + cat_target + "/lasso/lasso_MCOaaB.csv"
            import_temp = pd.read_csv(path_lasso)
            nomenc_to_keep = list(import_temp["Var"].head(how_many_vars))
            
        else :
            
            nomenc_to_keep = curated_vars["MCO"]["B"].copy()
            
        vars_to_keep = var_from_results(nomenc_to_keep)
        file_intermediate = path_snds + "T_MCO" + str(year) + "C.csv"
        file_MCOB = path_snds + "T_MCO" + str(year) + "B.csv"
        df_intermediate = pd.read_csv(file_intermediate, 
                                      sep = ";", 
                                      dtype = "object")
        
        df_intermediate = fc.clean_pmsi_aaC(df_intermediate, "MCO").copy()
        df_intermediate = df_intermediate[dic_join_vars["MCO"] + ["NUM_ENQ"]]
        
        df_mco_raw = pd.read_csv(file_MCOB, 
                                 sep = ";", 
                                 usecols = (vars_to_keep + dic_join_vars["MCO"]),
                                 dtype = "object")
        df_mco = fc.clean_pmsi_aaB(df_mco_raw, "MCO").copy()
        df_mco = pd.merge(left = df_mco, 
                          right = df_intermediate, 
                          how = "inner", 
                          on = dic_join_vars["MCO"])
        
    else:
        
        if (curated_vars == None) :
            
            path_lasso = "results/" + cat_target + "/lasso/lasso_MCOaa" + path_affine + ".csv"
            import_temp = pd.read_csv(path_lasso)
            nomenc_to_keep = list(import_temp["Var"].head(how_many_vars))
            try:
                import_temp_wotp = pd.read_csv("results/" + cat_target + "/lasso/lasso_wotp_MCOaa" + path_affine + ".csv")
                nomenc_to_keep_wotp = list(import_temp_wotp["Var"].head(how_many_vars))
                nomenc_to_keep.extend(nomenc_to_keep_wotp)
            except:
                pass
            
        else :
            
            nomenc_to_keep = curated_vars["MCO"][path_affine].copy()
            
        vars_to_keep = var_from_results(nomenc_to_keep)
        file_affine = path_snds + "T_MCO" + str(year) + path_affine + ".csv"
        df_mco_raw = pd.read_csv(file_affine, 
                                 sep = ";", 
                                 usecols = vars_to_keep + dic_join_vars["MCO"],
                                 dtype = "object", 
                                 encoding_errors="ignore")
        
        df_mco = fc.clean_pmsi_aaB(df_mco_raw, "MCO").copy()
        df_mco = fc.clean_pmsi_affine(df_mco).copy()
        vars_to_keep   = ["MCO" + "/" + path_affine + "/" + elem for elem in vars_to_keep]
        nomenc_to_keep = ["MCO" + "/" + path_affine + "/" + elem for elem in nomenc_to_keep]
        
        df_mco.columns = rename_affine(df_mco,
                                       "MCO",
                                       path_affine, 
                                       dic_join_vars["MCO"])
        
    return df_mco, vars_to_keep, nomenc_to_keep


def import_ssr(how_many_vars : int , 
               curated_vars : dict, 
               year: int,
               cat_target: str, 
               path_affine: str, 
               path_snds: str):
    
    """ 
    Importe les tables relatives au MCO, que ce soit T_MCOaaB ou les tables 
    affinées. Seules les nomenclatures de certaines variables retenues par la 
    première méthode lasso,  ou précisées dans curated_vars sont retenues afin 
    de limiter le temps d'importation

    Parameters
    ----------
    how_many_vars : int
        Combien de variables retenues par table depuis les résultats de Lasso,
        classées par ordre d'importance
    curated_vars : dict
        Si différent de None, outrepasse les résultats du Lasso pour fournir 
        soit-même une liste déterminée par analyse métier/à la main.
        Le format du dictionnaire attendu est donné dans globalvar
    year : int
        Année de la table importée
    cat_target : str
        Permet de déterminer les variables du Lasso correspondantes au
        phénotype étudié
    path_affine : str
        Chemin de la table affinée du PMSI_MCO, si path_affine = None alors on
        importe T_MCOaaB
    path_snds : str
        Chemin vers l'extraction du SNDS locale

    Returns
    -------
    df_mco : pd.DataFrame
        Table importée finale
    vars_to_keep : list de str
        Variables retenues par la feature selection/analyse métier pour 
        importation.
    nomenc_to_keep : list de str
        Nomenclatures des variables retenues.

    """
    
    if (path_affine == None):
        if (curated_vars == None) :
            
            path_lasso = "results/" + cat_target + "/lasso/lasso_SSRaaB.csv"
            import_temp = pd.read_csv(path_lasso)
            nomenc_to_keep = list(import_temp["Var"].head(how_many_vars))
            
        else :
            
            nomenc_to_keep = curated_vars["SSR"]["B"].copy()
            
        vars_to_keep = var_from_results(nomenc_to_keep)
        file_intermediate = path_snds + "T_SSR" + str(year) + "C.csv"
        file_SSRB = path_snds + "T_SSR" + str(year) + "B.csv"
        df_intermediate = pd.read_csv(file_intermediate, 
                                      sep = ";", 
                                      dtype = "object")
        
        df_intermediate = fc.clean_pmsi_aaC(df_intermediate, "SSR").copy()
        df_intermediate = df_intermediate[dic_join_vars["SSR"] + ["NUM_ENQ"]]
        
        df_ssr_raw = pd.read_csv(file_SSRB, 
                                 sep = ";", 
                                 usecols = (vars_to_keep + dic_join_vars["SSR_affine"]),
                                 dtype = "object")
        df_ssr = fc.clean_pmsi_aaB(df_ssr_raw, "SSR").copy()
        df_ssr = pd.merge(left = df_ssr, 
                          right = df_intermediate, 
                          how = "inner", 
                          on = dic_join_vars["SSR"])
        
        df_ssr = df_ssr.drop("RHS_NUM", axis = 1).drop_duplicates()
        
    else:
        
        if (curated_vars == None) :
            
            path_lasso = "results/" + cat_target + "/lasso/lasso_SSRaa" + path_affine + ".csv"
            import_temp = pd.read_csv(path_lasso)
            nomenc_to_keep = list(import_temp["Var"].head(how_many_vars))
            try:
                import_temp_wotp = pd.read_csv("results/" + cat_target + "/lasso/lasso_wotp_SSRaa" + path_affine + ".csv")
                nomenc_to_keep_wotp = list(import_temp_wotp["Var"].head(how_many_vars))
                nomenc_to_keep.extend(nomenc_to_keep_wotp)
            except:
                pass
            
        else :
            
            nomenc_to_keep = curated_vars["SSR"][path_affine].copy()
            
        vars_to_keep = var_from_results(nomenc_to_keep)
        file_affine = path_snds + "T_SSR" + str(year) + path_affine + ".csv"
        df_ssr_raw = pd.read_csv(file_affine, 
                                 sep = ";", 
                                 usecols = vars_to_keep + dic_join_vars["SSR"],
                                 dtype = "object", 
                                 encoding_errors="ignore")
        
        df_ssr = fc.clean_pmsi_aaB(df_ssr_raw, "SSR").copy()
        df_ssr = fc.clean_pmsi_affine(df_ssr).copy()
        vars_to_keep   = ["SSR" + "/" + path_affine + "/" + elem for elem in vars_to_keep]
        nomenc_to_keep = ["SSR" + "/" + path_affine + "/" + elem for elem in nomenc_to_keep]
        
        df_ssr.columns = rename_affine(df_ssr, 
                                       "SSR",
                                       path_affine, 
                                       dic_join_vars["SSR"])
        
    return df_ssr, vars_to_keep, nomenc_to_keep




#def import_ssr(how_many_vars, year, cat_target, path_affine, path_snds):
#    if (path_affine == None):
#        import_temp = pd.read_csv("results/" + cat_target + "/lasso/lasso_SSRaaB.csv") #None à changer au bout d'un moment
#        nomenc_to_keep = list(import_temp["Var"].head(how_many_vars))
#        vars_to_keep = var_from_results(nomenc_to_keep)
#        file_intermediate = path_snds + "T_SSR" + str(year) + "C.csv"
#        file_MCOB = path_snds + "T_SSR" + str(year) + "B.csv"
#        df_intermediate = pd.read_csv(file_intermediate, sep = ";", usecols = (dic_join_vars["SSR"] + ["NUM_ENQ"]), dtype = "object")
#        df_mco_raw = pd.read_csv(file_MCOB, sep = ";", usecols = (vars_to_keep + dic_join_vars["SSR"]), dtype = "object")
#        df_mco = fc.clean_pmsi_aaB(df_mco_raw, "SSR").copy()
#        df_mco = pd.merge(left = df_mco, right = df_intermediate, how = "inner", on = dic_join_vars["SSR"])
#    else:
#        import_temp = pd.read_csv("results/" + cat_target + "/lasso/lasso_SSRaa" + path_affine + ".csv")
#        nomenc_to_keep = list(import_temp["Var"].head(how_many_vars))
#        vars_to_keep = var_from_results(nomenc_to_keep)
#        file_affine = path_snds + "T_SSR" + str(year) + path_affine + ".csv"
#        df_mco_raw = pd.read_csv(file_affine, sep = ";", usecols = vars_to_keep + dic_join_vars["SSR"], dtype = "object") 
#        df_mco = fc.clean_pmsi_aaB(df_mco_raw, "SSR").copy()
#        df_mco = fc.clean_pmsi_affine(df_mco).copy()
#        vars_to_keep   = [path_affine + "~" + elem for elem in vars_to_keep]
#        nomenc_to_keep = [path_affine + "~" + elem for elem in nomenc_to_keep]
#        df_mco.columns = rename_affine(df_mco, path_affine, dic_join_vars["SSR"])
#    return df_mco, vars_to_keep, nomenc_to_keep

    

def flatten_dcir(affine_used : str,
                 how_many_vars: int, 
                 curated_vars: list[str], 
                 year: int, 
                 cat_target: str, 
                 path_snds:str):
    """
    Importe toutes les tables affinés et meilleures variables binarisées
    utilisées par la suite pour la régression XGBoost.

    Parameters
    ----------
    affine_used : str
        Tables affinées retenues.
    how_many_vars : int
        Nombres de meilleures variables binarisées utilisées (10 par exemple).
        Si curated_vars est défini alors ce paramètre est bypassé.
    curated_vars : list[str]
        Liste des variables binarisées sélectionnées à la main. La liste
        exacte est stockée dans globalvar.py
    year : int
        Année considérée.
    cat_target : str
        Phénotype étuide.
    path_snds : str
        Endroit où se situe les tables du SNDS sur le PC local.

    Returns
    -------
    df_clean : pd.DataFrame
        DESCRIPTION.
    var_inter : list[str]
        DESCRIPTION.

    """
    
    df_erprsf, vars_total, nomenc_total = import_dcir(how_many_vars, 
                                                      curated_vars, 
                                                      year, 
                                                      cat_target, 
                                                      None, 
                                                      path_snds)
    
    df_link = df_erprsf[["NUM_ENQ"] + dic_join_vars["DCIR"]]
    df_total = flatten_and_fill(df_erprsf, vars_total, nomenc_total)
    
    # Si pour une année donnée la valeur de nomenclature n'existe pas, 
    # on la rajoute artificiellement à 0.
    # Ceci afin de garantir des tailles de table d'entraînement constante
    nomenc_missing = list(set(nomenc_total).difference(set(df_total.columns)))
    df_total[nomenc_missing] = 0
    
    for path_affine in affine_used:
        df_temp, vars_temp, nomenc_temp = import_dcir(how_many_vars, 
                                                      curated_vars, 
                                                      year, 
                                                      cat_target, 
                                                      path_affine, 
                                                      path_snds)
        vars_total.extend(vars_temp)
        nomenc_total.extend(nomenc_temp)
        df_merge = pd.merge(left = df_link, 
                            right = df_temp,
                            on = dic_join_vars["DCIR"], 
                            how = "outer")
        df_notlazy_temp = flatten_and_fill(df_merge, 
                                           vars_temp, 
                                           nomenc_temp)
        
        nomenc_missing = list(set(nomenc_temp).difference(set(df_notlazy_temp.columns)))
        df_notlazy_temp[nomenc_missing] = 0
        
        df_total = pd.merge(left = df_total, 
                            right = df_notlazy_temp, 
                            on = ["NUM_ENQ"], 
                            how = "outer")
    var_inter = list(set(df_total.columns).intersection(set(nomenc_total)))
    var_inter.sort()
    df_clean = df_total[["NUM_ENQ"] + var_inter]
    
    return df_clean, var_inter


def flatten_mco(affine_used : str,
                how_many_vars: int, 
                curated_vars: list[str], 
                year: int, 
                cat_target: str, 
                path_snds:str):
    """
    Importe toutes les tables affinés et meilleures variables binarisées
    utilisées par la suite pour la régression XGBoost.

    Parameters
    ----------
    affine_used : str
        Tables affinées retenues.
    how_many_vars : int
        Nombres de meilleures variables binarisées utilisées (10 par exemple).
        Si curated_vars est défini alors ce paramètre est bypassé.
    curated_vars : list[str]
        Liste des variables binarisées sélectionnées à la main. La liste
        exacte est stockée dans globalvar.py
    year : int
        Année considérée.
    cat_target : str
        Phénotype étuide.
    path_snds : str
        Endroit où se situe les tables du SNDS sur le PC local.

    Returns
    -------
    df_clean : pd.DataFrame
        DESCRIPTION.
    var_inter : list[str]
        DESCRIPTION.

    """
    
    df_mco_b, vars_total, nomenc_total = import_mco(how_many_vars, 
                                                    curated_vars,  
                                                    year, 
                                                    cat_target, 
                                                    None, 
                                                    path_snds)
    df_link = df_mco_b[["NUM_ENQ"] + dic_join_vars["MCO"]]
    df_total = flatten_and_fill(df_mco_b, vars_total, nomenc_total)
    
    nomenc_missing = list(set(nomenc_total).difference(set(df_total.columns)))
    df_total[nomenc_missing] = 0
    
    for path_affine in affine_used:
        df_temp, vars_temp, nomenc_temp = import_mco(how_many_vars, 
                                                     curated_vars, 
                                                     year, 
                                                     cat_target, 
                                                     path_affine, 
                                                     path_snds)
        vars_total.extend(vars_temp)
        nomenc_total.extend(nomenc_temp)
        df_merge = pd.merge(left = df_link, 
                            right = df_temp, 
                            on = dic_join_vars["MCO"], 
                            how = "outer")
        df_notlazy_temp = flatten_and_fill(df_merge, vars_temp, nomenc_temp)
        
        nomenc_missing = list(set(nomenc_temp).difference(set(df_notlazy_temp.columns)))
        df_notlazy_temp[nomenc_missing] = 0
        
        df_total = pd.merge(left = df_total, 
                            right = df_notlazy_temp, 
                            on = ["NUM_ENQ"], 
                            how = "outer")
    var_inter = list(set(df_total.columns).intersection(set(nomenc_total)))
    var_inter.sort()
    df_clean = df_total[["NUM_ENQ"] + var_inter]
    
    return df_clean, var_inter


def flatten_ssr(affine_used : str,
                how_many_vars: int, 
                curated_vars: list[str], 
                year: int, 
                cat_target: str, 
                path_snds:str):
    """
    Importe toutes les tables affinés et meilleures variables binarisées
    utilisées par la suite pour la régression XGBoost.

    Parameters
    ----------
    affine_used : str
        Tables affinées retenues.
    how_many_vars : int
        Nombres de meilleures variables binarisées utilisées (10 par exemple).
        Si curated_vars est défini alors ce paramètre est bypassé.
    curated_vars : list[str]
        Liste des variables binarisées sélectionnées à la main. La liste
        exacte est stockée dans globalvar.py
    year : int
        Année considérée.
    cat_target : str
        Phénotype étuide.
    path_snds : str
        Endroit où se situe les tables du SNDS sur le PC local.

    Returns
    -------
    df_clean : pd.DataFrame
        DESCRIPTION.
    var_inter : list[str]
        DESCRIPTION.

    """
    
    df_ssr_b, vars_total, nomenc_total = import_ssr(how_many_vars, 
                                                    curated_vars,
                                                    year, 
                                                    cat_target, 
                                                    None, 
                                                    path_snds)
    
    
    df_total = flatten_and_fill(df_ssr_b, vars_total, nomenc_total)
    nomenc_missing = list(set(nomenc_total).difference(set(df_total.columns)))
    df_total[nomenc_missing] = 0
    
    for path_affine in affine_used:
        df_temp, vars_temp, nomenc_temp = import_ssr(how_many_vars, 
                                                     curated_vars,
                                                     year, 
                                                     cat_target,
                                                     path_affine, 
                                                     path_snds)
        vars_total.extend(vars_temp)
        nomenc_total.extend(nomenc_temp)
        try:
            df_link = df_ssr_b[["NUM_ENQ"] + dic_join_vars["SSR_affine"]]
            df_merge = pd.merge(left = df_link, 
                                right = df_temp, 
                                on = dic_join_vars["SSR_affine"], 
                                how = "outer")
            df_merge = df_merge.drop("RHS_NUM", axis = 1).drop_duplicates()
            
        except:
            df_link = df_ssr_b[["NUM_ENQ"] + dic_join_vars["SSR"]]
            df_merge = pd.merge(left = df_link, 
                                right = df_temp, 
                                on = dic_join_vars["SSR"], 
                                how = "outer")
            
        df_notlazy_temp = flatten_and_fill(df_merge, 
                                           vars_temp, 
                                           nomenc_temp)
        
        nomenc_missing = list(set(nomenc_temp).difference(set(df_notlazy_temp.columns)))
        df_notlazy_temp[nomenc_missing] = 0
        
        df_total = pd.merge(left = df_total, 
                            right = df_notlazy_temp, 
                            on = ["NUM_ENQ"], 
                            how = "outer")
    var_inter = list(set(df_total.columns).intersection(set(nomenc_total)))
    var_inter.sort()
    df_clean = df_total[["NUM_ENQ"] + var_inter]
    return df_clean, var_inter


def flatten_ald(cat_target: str,
                curated_vars: list[str], 
                how_many_vars: int, 
                year: int) -> pd.DataFrame:
    """
    Importe la table IR_IMB_R et l'aplatit pour utilisation par le XGBoost

    Parameters
    ----------
    cat_target : str
        Phénotype.
    curated_vars : list[str]
        Variables binarisées utilisées.
    how_many_vars : int
        Nombre de variable retenues depuis le Lasso.
    year : int
        Année considérée.

    Returns
    -------
    flat_ald : TYPE
        Table annuelle aplatie pour XGBoost.

    """
    df_ald, varna = fs.import_ald()
    
    if curated_vars == None:
        path_ir_imb_r = "results/" + cat_target + "/lasso/lasso_IR_IMB_R.csv"
        import_temp = pd.read_csv(path_ir_imb_r)
        nomenctoflattend = list(import_temp["Var"][:how_many_vars])
    
    else : 
        nomenctoflattend = curated_vars["ALD"]
        
    df_ald_oneyear = fs.yearisation_ald(df_ald, int("20" + str(year)))
    df_snds = df_ald_oneyear[["NUM_ENQ", "MED_MTF_COD"]].copy().drop_duplicates()
    flat_ald = flatten_and_fill(df_snds, 
                                ["MED_MTF_COD"], 
                                nomenctoflattend)
    
    nomenc_missing = list(set(nomenctoflattend).difference(set(flat_ald.columns)))
    flat_ald[nomenc_missing] = 0
    
    flat_ald = flat_ald[["NUM_ENQ"] + nomenctoflattend]
    
    return flat_ald



# =============================================================================
# Datamanagement en vue du Machine Learning
# =============================================================================

def create_snds_oneyear(year: int, 
                        affine_used: dict, 
                        how_many_vars: int, 
                        curated_vars: dict, 
                        cat_target: str, 
                        path_snds: str):
    """
    Créer une année aplatie du SNDS avec tous les NUM_ENQ possibles

    Parameters
    ----------
    year : int
        DESCRIPTION.
    affine_used : dict
        DESCRIPTION.
    how_many_vars : int
        DESCRIPTION.
    curated_vars : dict
        DESCRIPTION.
    cat_target : str
        DESCRIPTION.
    path_snds : str
        DESCRIPTION.

    Returns
    -------
    data : TYPE
        DESCRIPTION.
    var_predict : TYPE
        DESCRIPTION.

    """
    
    flat_ald = flatten_ald(cat_target, 
                           curated_vars, 
                           how_many_vars, 
                           year)
    
    flat_dcir, var_inter = flatten_dcir(affine_used["DCIR"], 
                                        how_many_vars, 
                                        curated_vars, 
                                        year, 
                                        cat_target, 
                                        path_snds)
    
    flat_mco, var_inter = flatten_mco(affine_used["MCO"], 
                                      how_many_vars, 
                                      curated_vars,
                                      year, 
                                      cat_target, 
                                      path_snds)
    
    df_merge = pd.merge(left = flat_dcir, 
                        right = flat_mco, 
                        on = "NUM_ENQ", 
                        how = "outer").fillna(0)
    
    if ("SSR" in affine_used.keys()):
        flat_ssr, var_inter = flatten_ssr(affine_used["SSR"], 
                                          how_many_vars, 
                                          curated_vars,
                                          year, 
                                          cat_target, 
                                          path_snds)

        df_merge = pd.merge(left = df_merge, 
                            right = flat_ssr, 
                            on = "NUM_ENQ", 
                            how = "outer").fillna(0)
        
    df_merge = pd.merge(left = df_merge, 
                        right = flat_ald, 
                        on = "NUM_ENQ", 
                        how = "outer").fillna(0)
    
    data = fs.merge_status(df_merge, cat_target)
    
    var_predict = list(df_merge.columns)[1:]

    return data, var_predict

def create_snds_allyear_allnumenq(range_year, 
                                  affine_used: dict, 
                                  how_many_vars: int, 
                                  curated_vars: dict, 
                                  cat_target: str, 
                                  path_snds: str) -> None:
    """
    Créer les bases annuelles applaties pour tout l'échantillon.

    Parameters
    ----------
    range_year : TYPE
        Range d'années que l'on veut créer, on peut aller de 2012 à 2021 max.
    affine_used : dict
        Tables affinées utilisées.
    how_many_vars : int
        Nombre de variables issues du Lasso.
    curated_vars : dict
        DESCRIPTION.
    cat_target : str
        DESCRIPTION.
    path_snds : str
        DESCRIPTION.

    Returns
    -------
    None
        Ne retourne rien, se contente de créer les données.

    """
    for myyear in range_year:
        data, var_predict = create_snds_oneyear(myyear, 
                                                affine_used, 
                                                how_many_vars, 
                                                curated_vars, 
                                                cat_target, 
                                                path_snds)  
        path_result = "results/" + cat_target + "/data/all_numenq_" + str(myyear) + ".csv"
        data.to_csv(path_result, sep = ";", index = False)
        
        print(f"Flatten SNDS created for year {myyear}")
        
def create_dateset_oneyear(snds_hepather, 
                           affine_used, 
                           how_many_vars, 
                           curated_vars, 
                           cat_target, 
                           path_snds):
    
    if cat_target in ["fibrose_F4_B", "fibrose_F4_C"]:
        date_used = "date_fibrose"
    
    else:
        date_used = "date_incl"
        
    snds_cat = snds_hepather[snds_hepather[cat_target + "_champ"] == 1].copy()
    snds_cat = snds_cat[~snds_cat[date_used].isna()]
    snds_cat["year_used"] =  snds_cat[date_used].apply(lambda x: datetime.strptime(x, "%Y-%m-%d").year)
    #snds_cat = snds_cat[snds_cat["year_used"].isin([2013, 2014, 2015])]
    
    df_toconcat = []
    for myyear in range(13, 16):
        # Les dates d'inclusions sont restreintes à 2013-2015 Hepather
        # 2012 pas inclus car i) même pas 100 personnes, et ii) si on utilise
        # les données fenetrées un an avant l'inclusion on ne peut pas les 
        # prendre en compte (SNDS 2011 pas dispo)
        snds_temp = snds_cat[snds_cat["year_used"] == 2000 + myyear]
        numenq_temp = list(snds_temp["NUM_ENQ"])
        path_results = "results/" + cat_target + "/data/all_numenq_" + str(myyear) + ".csv"
        df_temp = pd.read_csv(path_results, sep = ";")
        df_temp = df_temp[df_temp["NUM_ENQ"].isin(numenq_temp)]
        df_toconcat.append(df_temp)
    
    df_final = pd.concat(df_toconcat)
    var_predict = list(df_final.columns[1:-2])
    
    return df_final, var_predict
    
def create_dateset_threeyears(type_agreg,
                              snds_hepather, 
                              affine_used, 
                              how_many_vars, 
                              curated_vars, 
                              cat_target, 
                              path_snds):
    
    if cat_target in ["fibrose_F4_B", "fibrose_F4_C"]:
        date_used = "date_fibrose"
    
    else:
        date_used = "date_incl"
    
    snds_cat = snds_hepather[snds_hepather[cat_target + "_champ"] == 1].copy()
    snds_cat = snds_cat[~snds_cat[date_used].isna()]
    snds_cat["year_used"] =  snds_cat[date_used].apply(lambda x: datetime.strptime(x, "%Y-%m-%d").year)
    
    df_toconcat = []
    for myyear in range(13, 16):
        snds_temp = snds_cat[snds_cat["year_used"] == 2000 + myyear]
        numenq_temp = list(snds_temp["NUM_ENQ"])
        path_results = "results/" + cat_target + "/data/all_numenq_" 
        df_temp_back = pd.read_csv(path_results + str(myyear - 1) + ".csv", 
                                   sep = ";")
        df_temp      = pd.read_csv(path_results + str(myyear) + ".csv", 
                                   sep = ";")
        df_temp_forw = pd.read_csv(path_results + str(myyear + 1) + ".csv", 
                                   sep = ";")
        df_temp = pd.concat([df_temp_back, df_temp, df_temp_forw])
        df_temp = df_temp[df_temp["NUM_ENQ"].isin(numenq_temp)]
        if (type_agreg == "mean"):
            df_temp = df_temp.groupby("NUM_ENQ", as_index = False).mean()
        if (type_agreg == "max"):
            df_temp = df_temp.groupby("NUM_ENQ", as_index = False).max()
        df_toconcat.append(df_temp)
    
    df_final = pd.concat(df_toconcat).reset_index().drop("index", axis = 1)
    
    var_predict = list(df_final.columns[1:-2])
    
    return df_final, var_predict

def create_dataset_span(span_evaluation, 
                        affine_used, 
                        how_many_vars, 
                        curated_vars, 
                        cat_target, 
                        path_snds):
    
    save_flat_ald = []
    for myyear in span_evaluation:
        flat_ald_temp = flatten_ald(cat_target, 
                                    curated_vars, 
                                    how_many_vars, 
                                    year = myyear)
        save_flat_ald.append(flat_ald_temp)
    flat_ald = pd.concat(save_flat_ald)
    flat_ald = flat_ald.groupby("NUM_ENQ", as_index = False).max()

    save_flat_dcir = []
    for myyear in span_evaluation:
        flat_dcir_temp, var_inter = flatten_dcir(affine_used["DCIR"], 
                                                 how_many_vars, 
                                                 curated_vars, 
                                                 myyear, 
                                                 cat_target, 
                                                 path_snds)
        save_flat_dcir.append(flat_dcir_temp)
    flat_dcir = pd.concat(save_flat_dcir)
    flat_dcir = flat_dcir.groupby("NUM_ENQ", as_index = False).max()
    
    save_flat_mco = []
    for myyear in span_evaluation:
        flat_mco_temp, var_inter = flatten_mco(affine_used["MCO"],
                                               how_many_vars, 
                                               curated_vars, 
                                               myyear, 
                                               cat_target, 
                                               path_snds)
        save_flat_mco.append(flat_mco_temp)
    flat_mco = pd.concat(save_flat_mco)
    flat_mco = flat_mco.groupby("NUM_ENQ", as_index = False).max()
    
    if ("SSR" in affine_used.keys()):
        save_flat_ssr = []
        for myyear in span_evaluation:
            flat_ssr_temp, var_inter = flatten_ssr(affine_used["SSR"], 
                                                   how_many_vars, 
                                                   myyear, 
                                                   cat_target, 
                                                   path_snds)
            save_flat_ssr.append(flat_ssr_temp)
        flat_ssr = pd.concat(save_flat_ssr)
        flat_ssr = flat_ssr.groupby("NUM_ENQ", as_index = False).max()
    
    df_merge = pd.merge(left = flat_dcir, 
                        right = flat_mco, 
                        on = "NUM_ENQ", 
                        how = "outer").fillna(0)
    if ("SSR" in affine_used.keys()):
        df_merge = pd.merge(left = df_merge,
                            right = flat_ssr,
                            on = "NUM_ENQ",
                            how = "outer").fillna(0)
    df_merge = pd.merge(left = df_merge, 
                        right = flat_ald, 
                        on = "NUM_ENQ", 
                        how = "outer").fillna(0)
    data = fs.merge_status(df_merge, cat_target)
    var_predict = list(df_merge.columns)[1:]

    return data, var_predict


def prepare_data(cat_target: str,
                 var_predict: list[str],
                 data: pd.DataFrame,
                 size_train: float):
    """
    Fonction pour séparer train et test ainsi que faire du resampling dans
    l'échantillon d'entraînement pour équilibrer observations selon le
    phénotype étudié.
    
    Parameters
    ----------
    cat_target : str
        Phénotype étudié.
    var_predict : list[str]
        Liste de variables binarisées utilisées pour la prédiction.
    data : pd.DataFrame
        Base finale applatie utilisée.
    size_train : float
        Pourcentage utilisé pour l'entraînement en cross validation.

    Returns
    -------
    X_train : pd.DataFrame
        Echantillon d'entraînement.
    y_train : pd.DataFrame
        Statut à prédire pour l'entraînement.
    X_test : pd.DataFrame
        Echantillon de validation.
    y_test : pd.DataFrame
        Statut à prédire pour la validation.
    num_enq_train : TYPE
        NUM_ENQ utilisé pour le train, garder une trace pour ne pas avoir de
        data leaking.

    """
    
    num_enq_train = list(data["NUM_ENQ"].sample(frac = size_train))
    data_holdout = data[~data["NUM_ENQ"].isin(num_enq_train)]
    data_train = data[data["NUM_ENQ"].isin(num_enq_train)]
    data_resampled = fs.resample_data(data_train, cat_target)
    
    X_train, y_train = data_resampled[var_predict], data_resampled[cat_target]
    X_test, y_test = data_holdout[var_predict], data_holdout[cat_target]

    return X_train, y_train, X_test, y_test, num_enq_train



