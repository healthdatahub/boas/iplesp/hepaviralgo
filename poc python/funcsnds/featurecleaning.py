# -*- coding: utf-8 -*-
"""
Nettoyage des tables PMSI/DCIR selon les règles usuelles, par exemple celles
décrites pour SCALPEL-Extraction sur la documentation SNDS du HDH.
Les autres corrections consistent essentiellement à remplacer des valeurs
manquantes par "MISSING" ou à s'assurer que le format attendu (string d'un
certain nombre de caractères) est respecté.
"""

# =============================================================================
# Packages et variables utiles
# =============================================================================

import pandas as pd
import funcsnds.globalvar as gv

# Numero des établissement en double à retirer systématiquement

ETA_NUM_doubles = [
'130780521', '130783236', '130783293', '130784234', '130804297', '600100101',
'750041543', '750100018', '750100042', '750100075', '750100083', '750100091', 
'750100109', '750100125', '750100166', '750100208', '750100216', '750100232', 
'750100273', '750100299', '750801441', '750803447', '750803454', '910100015', 
'910100023', '920100013', '920100021', '920100039', '920100047', '920100054',
'920100062', '930100011', '930100037', '930100045', '940100027', '940100035', 
'940100043', '940100050', '940100068', '950100016', '690783154', '690784137', 
'690784152', '690784178', '690787478', '830100558' 
]

# Utilisation des correspondances ATC_UCD_CIP pour passer d'un code à l'autre

path_nomenc_pha = "C:/Users/Titouan/Documents/HepavirAlgo/data/nomenc/IR_PHA_R.csv"

nomenc_pha = pd.read_csv(path_nomenc_pha, 
                         sep = ";", 
                         usecols = ["PHA_CIP_C13", 
                                    "PHA_CIP_UCD", 
                                    "PHA_MED_NOM", 
                                    "PHA_ATC_C03", 
                                    "PHA_ATC_C07"],
                         dtype = "object")

nomenc_pha["PHA_CIP_UCD"] = nomenc_pha["PHA_CIP_UCD"].apply(lambda x: x.zfill(13))

mapping_cip_atc = nomenc_pha[["PHA_CIP_C13", "PHA_ATC_C07"]].set_index("PHA_CIP_C13").to_dict()["PHA_ATC_C07"]
mapping_ucd_atc = nomenc_pha[["PHA_CIP_UCD", "PHA_ATC_C07"]].set_index("PHA_CIP_UCD").to_dict()["PHA_ATC_C07"]

var_retour_pmsi = gv.var_retour_pmsimco
   
# =============================================================================
# Nettoyage du DCIR   
# =============================================================================
   
def clean_erprsf(df_erprsf: pd.DataFrame) -> pd.DataFrame:
    """
    Nettoyage standard de ER_PRS_F
    
    Parameters
    ----------
    df_erprsf : pd.DataFrame
        ER_PRS_F brut.

    Returns
    -------
    df_erprsf : TYPE
        ER_PRS_F nettoyé.

    """
    
    # Suppression des lignes pour remontées d'information
    df_erprsf = df_erprsf[df_erprsf["DPN_QLF"] != "71"]
    # Lignes de majorations exclues car on cherche à dénombrer les prestations
    df_erprsf = df_erprsf[df_erprsf["CPL_MAJ_TOP"] != "2"]
    # Suppression des lignes dont la nature de la prestation est sans objet
    # mydf = mydf[mydf["BSE_PRS_NAT"] != "0"]
    # Cela semble faire la même chose que CPL MAJ TOP != 2
    
    return df_erprsf

def clean_dcir_affine(mydf: pd.DataFrame) -> pd.DataFrame:
    
    """
    Modification au besoin de tables affinées du DCIR. Notamment traduction des
    nomenclatures de médicaments et imputation de valeurs manquantes.
    
    Parameters
    ----------
    mydf : pd.DataFrame
        Table affinée brute du DCIR

    Returns
    -------
    mydf : TYPE
        Table affinée modifiée selon besoins

    """
    
    # Pour s'assurer qu'il n'y ait pas de valeur manquante pour ces variables
    if ("ETE_NAT_FSJ" in mydf.columns):
        mydf["ETE_NAT_FSJ"].fillna("MISSING", inplace = True)
        
    if ("PHA_SEQ_RNV" in mydf.columns):
        mydf["PHA_SEQ_RNV"].fillna("MISSING", inplace = True)
    
    if ("PHA_PRS_C13" in mydf.columns):
        mydf["PHA_PRS_C13"] = mydf["PHA_PRS_C13"].map(mapping_cip_atc)
        mydf["PHA_PRS_C13"] = mydf["PHA_PRS_C13"].apply(lambda x: str(x)[:5])
    
    #CHANGEMENT    
    #if ("UCD_UCD_COD" in mydf.columns):
    #    mydf["UCD_UCD_COD"] = mydf["UCD_UCD_COD"].map(mapping_ucd_atc)
    #    mydf["UCD_UCD_COD"] = mydf["UCD_UCD_COD"].apply(lambda x: str(x)[:5])
    
    # Artefact des premières explorations, si l'on désirait segmenter la 
    # variable en quantile pour ensuite l'utiliser comme variable catégorielle
    # if ("CAM_ACT_PRU" in mydf.columns):
    #     mydf["CAM_ACT_PRU_cat"] = pd.qcut(mydf["CAM_ACT_PRU"], q = 100, 
    #                                       duplicates = "drop")
        
    return mydf   

# =============================================================================
# Nettoyage du PMSI-MCO/SSR
# =============================================================================

def clean_pmsi_aaC(pmsi_raw: pd.DataFrame, type_snds: str) -> pd.DataFrame:
    
    """
    Nettoyage des tables de jointures "aaC" du PMSI (MCO ou SSR)
    
    Parameters
    ----------
    pmsi_raw : pd.DataFrame
        Table "aaC" brute du PMSI
        
    type_snds : str
        Type de PMSI (MCO ou SSR)

    Returns
    -------
    pmsi_clean : pd.DataFrame
        Table "aaC" traitée du PMSI

    """    
    
    # Supression standard des établissements avec ETA_NUM en double
    pmsi_clean = pmsi_raw[~pmsi_raw["ETA_NUM"].isin(ETA_NUM_doubles)].copy()
    
    
    # Codes retours valant zéro supprimés
    if (type_snds == "MCO"):
        for var_ret in var_retour_pmsi:
            if (var_ret in pmsi_clean.columns):
                pmsi_clean = pmsi_clean[(pmsi_clean[var_ret] == "0")]

    # Cette boucle permet de remplacer ce code inélégant :
    # pmsi_clean = pmsi_clean[(pmsi_clean["COH_NAI_RET"] == "0") &
    #                         (pmsi_clean["COH_SEX_RET"] == "0") &
    #                         (pmsi_clean["DAT_RET"] == "0") &
    #                         (pmsi_clean["FHO_RET"] == "0") &
    #                         (pmsi_clean["HOS_NNE_RET"] == "0") &
    #                         (pmsi_clean["HOS_ORG_RET"] == "0") &
    #                         (pmsi_clean["NAI_RET"] == "0") &
    #                         (pmsi_clean["NIR_RET"] == "0") &
    #                         (pmsi_clean["PMS_RET"] == "0") &
    #                         (pmsi_clean["SEJ_MER_RET"] == "0") &
    #                         (pmsi_clean["SEJ_RET"] == "0") &
    #                         (pmsi_clean["SEX_RET"] == "0")]
    
    if (type_snds == "SSR") :
        if ("GRG_GME" in pmsi_clean.columns):
            pmsi_clean = pmsi_clean[pmsi_clean["GRG_GME"].str[0:2] != "90"].copy()
        if ("FP_PEC" in pmsi_clean.columns):
            pmsi_clean["FP_PEC"].fillna("MISSING", inplace = True)
        if ("MOR_PRP" in pmsi_clean.columns):
            pmsi_clean["MOR_PRP"].fillna("MISSING", inplace = True)
        if ("ETL_AFF" in pmsi_clean.columns):
            pmsi_clean["ETL_AFF"].fillna("MISSING", inplace = True)
    
    return pmsi_clean


def clean_pmsi_aaB(pmsi_raw: pd.DataFrame, type_snds: str) -> pd.DataFrame:

    """
    Nettoyage des tables de jointures "aaB" du PMSI (MCO ou SSR)
    
    Parameters
    ----------
    pmsi_raw : pd.DataFrame
        Table "aaB" brute du PMSI
        
    type_snds : str
        Type de PMSI (MCO ou SSR)

    Returns
    -------
    pmsi_clean : pd.DataFrame
        Table "aaB" traitée du PMSI

    """    
    
    pmsi_clean = pmsi_raw[~pmsi_raw["ETA_NUM"].isin(ETA_NUM_doubles)].copy()
    
    if (type_snds == "MCO"):
        if ("GRG_GHM" in pmsi_clean.columns):
            pmsi_clean = pmsi_clean[pmsi_clean["GRG_GHM"].str[0:2] != "90"].copy()
            pmsi_clean["GRG_GHM"] = pmsi_clean["GRG_GHM"].apply(lambda x: x[:5])
        if ("SEJ_TYP" in pmsi_clean.columns):
            pmsi_clean = pmsi_clean[pmsi_clean["SEJ_TYP"] != "B"].copy()
        if ("DGN_REL" in pmsi_clean.columns):
            pmsi_clean["DGN_REL"].fillna("MISSING", inplace = True)
            
    
    if (type_snds == "SSR") :
        if ("GRG_GME" in pmsi_clean.columns):
            pmsi_clean = pmsi_clean[pmsi_clean["GRG_GME"].str[0:2] != "90"].copy()
        if ("FP_PEC" in pmsi_clean.columns):
            pmsi_clean["FP_PEC"].fillna("MISSING", inplace = True)
        if ("MOR_PRP" in pmsi_clean.columns):
            pmsi_clean["MOR_PRP"].fillna("MISSING", inplace = True)
        if ("ETL_AFF" in pmsi_clean.columns):
            pmsi_clean["ETL_AFF"].fillna("MISSING", inplace = True)
            
    return pmsi_clean



def clean_pmsi_affine(mydf: pd.DataFrame) -> pd.DataFrame:
    
    """
    Nettoyage des tables affinées ddu PMSI (MCO ou SSR)
    
    Parameters
    ----------
    mydf : pd.DataFrame
        Table affinée brute du PMSI
        
    Returns
    -------
    mydf : pd.DataFrame
        Table affinée traitée du PMSI

    """    

    if ("NABM_COD" in mydf.columns):
        mydf = mydf[mydf["NABM_COD"] != "1      0"].copy()
        mydf = mydf[mydf["NABM_COD"] != "0      0"].copy()
        mydf["NABM_COD"] = mydf["NABM_COD"].apply(lambda x : x.zfill(8))
    if ("COD_UCD" in mydf.columns):
        mydf["COD_UCD"] = mydf["COD_UCD"].apply(str)
    if ("PSH_DMT" in mydf.columns):
        mydf["PSH_DMT"].fillna("MISSING", inplace = True)    
    if ("DGN_REL" in mydf.columns):
        mydf = mydf[mydf["DGN_REL"] != "xxxx"].copy()
        mydf["DGN_REL"].fillna("MISSING", inplace = True) 
    if ("DGN_PAL" in mydf.columns):
        mydf = mydf[mydf["DGN_PAL"] != "RSSABS"].copy()
        mydf["DGN_PAL"].fillna("MISSING", inplace = True) 
    if ("PSH_MDT" in mydf.columns):
        mydf["PSH_MDT"].fillna("MISSING", inplace = True) 
        
    return mydf   



