# -*- coding: utf-8 -*-
"""
Fonctions pour trouver des variables pertinentes dans le DCIR
"""

import pandas as pd
import numpy as np
import polars as pl
import pyarrow as pa
from sklearn.linear_model import Lasso
from sklearn.utils import resample
from datetime import datetime
import funcsnds.featurecleaning as fc

dic_join_vars = {
    "DCIR" : ["FLX_DIS_DTD", "FLX_TRT_DTD", "FLX_EMT_TYP", "FLX_EMT_NUM", "FLX_EMT_ORD", "ORG_CLE_NUM", "DCT_ORD_NUM", "PRS_ORD_NUM", "REM_TYP_AFF"],
    "MCO"  : ["ETA_NUM", "RSA_NUM"],
    "SSR"  : ["ETA_NUM", "RHA_NUM"]
    }

exclusion_phenotype = {
    "hepatite_reco_B" : ["B", "C"],
    "hepatite_reco_C" : ["B", "C"],
    "cirrhose_B" : ["B"],
    "cirrhose_C" : ["C"],
    "fibrose_F4_B" : ["B"],
    "fibrose_F4_C" : ["C"],
    "HEPAD" : ["B", "BC"],
    "sain_B" : ["B"]
    }

# listaffine = []
# for elem in os.listdir("D:\Data\HEPAT_VUE_30012023\HEP_CSV"):
#     if ("ER_" in elem):
#         listaffine.append(elem.split("_")[1])
# list(set(listaffine))

list_dcir = ['BIO',
 'ARO',
 'PHA',
 'PRS',
 'UCD',
 'DTR',
 'TRS',
 'TIP',
 'RAT',
 'CAM',
 'INV',
 'ETE']

def one_dcir_affine(year, list_affine = list_dcir):
    dict_df_affine = {}
    for elem in list_affine:
        if elem == "PRS" :
            df_erprsf = pd.read_csv("D:/Data/HEPAT_VUE_30012023/HEP_CSV/ER_" + elem + "_F_20"+ str(year) + ".CSV", usecols = ["NUM_ENQ", "PRS_NAT_REF", "PSP_SPE_COD", "PSE_SPE_COD", "CPL_MAJ_TOP", "BSE_PRS_NAT", "DPN_QLF", "PRS_ACT_QTE"] + dic_join_vars["DCIR"], sep = ";", dtype = "object")
        else:
            dict_df_affine[elem] = pd.read_csv("D:/Data/HEPAT_VUE_30012023/HEP_CSV/ER_" + elem + "_F_20"+ str(year) + ".CSV", sep = ";", dtype = "object")
    return df_erprsf, dict_df_affine

def merge_affine(df_one, dfaffine):
    temp = pd.merge(left = df_one, right = dfaffine, on = dic_join_vars["DCIR"])
    if temp.shape[0] == 0:
        print("Pas de données")

    return temp

def merge_affine_mco(df_one, dfaffine):
    temp = pd.merge(left = df_one, right = dfaffine, on = dic_join_vars["MCO"])
    if temp.shape[0] == 0:
        print("Pas de données")

    return temp