# -*- coding: utf-8 -*-
"""
Fonctions pour trouver des variables pertinentes dans le DCIR
"""

import pandas as pd
import numpy as np
import polars as pl
import pyarrow as pa
from sklearn.linear_model import Lasso
from sklearn.utils import resample
from datetime import datetime
import funcsnds.featurecleaning as fc

def display_nomenc(results, type_nomenc, type_import = "pandas"):
    if (type_nomenc == "BIO"):
        
        nomenc = pd.read_csv("C:/Users/Titouan/Documents/HepavirAlgo/data/nomenc/IR_BIO_R.csv", sep = ";", usecols = ["BIO_PRS_IDE", "BIO_INF_LIB"], dtype = "object")
        nomenc["BIO_PRS_IDE"] = nomenc["BIO_PRS_IDE"].apply(lambda x: "NABM_COD/" + x.zfill(8))
        nomenc.columns = ["Var", "Descriptif"]
    
    if (type_nomenc == "UCD"):
        
        nomenc = pd.read_csv("C:/Users/Titouan/Documents/HepavirAlgo/data/nomenc/NU_UCD_R.csv", sep = ";", usecols = ["UCD_UCD_COD", "UCD_DSN_LIB"], dtype = "object")
        nomenc["UCD_UCD_COD"] = nomenc["UCD_UCD_COD"].apply(lambda x: "UCD_UCD_COD/" + str(x))

    if (type_nomenc == "CCAM"):
        nomenc = pd.read_csv("C:/Users/Titouan/Documents/HepavirAlgo/data/nomenc/IR_CCAM_V63.csv", sep = ";", usecols = ["CAM_PRS_IDE_COD", "CAM_PRS_IDE_LIB"], dtype = "object")
        nomenc["CAM_PRS_IDE_COD"] = nomenc["CAM_PRS_IDE_COD"].apply(lambda x: "CCAM_COD/" + x)
        
    if (type_nomenc == "CDC_ACT"):
        nomenc = pd.read_csv("C:/Users/Titouan/Documents/HepavirAlgo/data/nomenc/IR_CCAM_V63.csv", sep = ";", usecols = ["CAM_PRS_IDE_COD", "CAM_PRS_IDE_LIB"], dtype = "object")
        nomenc["CAM_PRS_IDE_COD"] = nomenc["CAM_PRS_IDE_COD"].apply(lambda x: "CDC_ACT/" + x)    
    
    if (type_nomenc == "ASS_DGN"):
        nomenc = pd.read_csv("C:/Users/Titouan/Documents/HepavirAlgo/data/nomenc/MS_CIM_V.csv", sep = ";", usecols = ["CIM_COD", "CIM_LIL"], dtype = "object")
        nomenc["CIM_COD"] = nomenc["CIM_COD"].apply(lambda x: "ASS_DGN/" + x)    
            
        
    if (type_nomenc == "DGN"):
        
        nomenc = pd.read_csv("C:/Users/Titouan/Documents/HepavirAlgo/data/nomenc/MS_CIM_V.csv", sep = ";", usecols = ["CIM_COD", "CIM_LIL"], dtype = "object")
        nomenc_bis = nomenc.copy()
        nomenc["CIM_COD"] = nomenc["CIM_COD"].apply(lambda x: "DGN_PAL/" + x)
        nomenc_bis["CIM_COD"] = nomenc_bis["CIM_COD"].apply(lambda x: "DGN_REL/" + x)
        nomenc.columns = ["Var", "Descriptif"]
        nomenc_bis.columns = ["Var", "Descriptif"]
        
        nomenc = pd.concat([nomenc, nomenc_bis], axis = 0)
        
        results_nomenc = pd.merge(left = results, right = nomenc, on = "Var", how = "left")
        return results_nomenc
    
    nomenc.columns = ["Var", "Descriptif"]
    results_nomenc = pd.merge(left = results, right = nomenc, on = "Var")
    return results_nomenc