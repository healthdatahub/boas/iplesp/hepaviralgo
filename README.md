# HepavirAlgo

## Titre du projet :

Algorithmes de ciblage de patients atteints d'hépatite virale B et C chronique, ainsi que de phénotypes associés à ces maladies (cirrhoses, porteurs dits “sains”, hépatite Delta).

## Auteurs : 

IPLESP, personne référente Fabien Carrat (fabrice.carrat@iplesp.upmc.fr) et pour le HDH, [Titouan Blaize](https://www.linkedin.com/in/titouan-blaize-34906781/) (titouan.blaize@health-data-hub.fr) 

## Objectifs de l'algorithme

Les algorithmes HepavirAlgo sont des algorithmes de ciblage appliqués au SNDS afin de constituer des cohortes et sous-groupes spécifiques de malades en France. Les pathologies et sous pathologies sont ici : hépatite virale chronique B et C (appelés VHB et VHC chroniques), cirrhose parmi les patients VHB et VHC chroniques précedemment repérés, variant hépatique Delta parmi les VHB chroniques et porteurs sains VHB chroniques. Ces algorithmes doivent permettre d’estimer la prévalence de ces pathologies ou sous-pathologies au niveau national et régional, les trajectoires de soins associées, les consommations de soins et de biens médicaux résultantes.

De précédents travaux appliqués à des bases médico-administratives afin d’identifier les hépatites virales chroniques ont déjà été réalisés. La [cartographie des pathologies de la CNAM](https://www.assurance-maladie.ameli.fr/content/etude-des-algorithmes-de-definition-des-pathologies-dans-le-sniiram-partie-1) propose un certain nombre de règles de classification de maladie pour les hépatites virales chroniques. Plus récemment, la validité de certains de ces algorithmes a pu être testée sur le SNDS, en les comparant au statut virologique issu d’une cohorte HEPATHER [(Lam, 2023)](https://onlinelibrary.wiley.com/doi/full/10.1111/jvh.13788). Les algorithmes les plus simples intègrent essentiellement la nomenclature CIM-10 codée lors des séjours. Ces algorithmes présentent une excellente spécificité mais des sensibilités modérées. La sensibilité des algorithmes était généralement améliorée après prise en compte de tests biologiques relatifs aux malades avec une hépatite virale, la délivrance de médicaments spécifiques à l’hépatite B ou C, la prise en compte des notifications d’ALD.

## Méthodologie :

Les objectifs et enjeux du projet HepavirAlgo sont multiples. Tout d'abord, ce projet consiste en l’utilisation des méthodes de machine learning afin d'améliorer les performances prédictives des travaux cités précédemment. L'intérêt de ces méthodes réside dans (i) la détection agnostique, automatique et sans expertise métier des variables d'intérêt (_feature selection_) et (ii) la combinaison de ces variables de manière non linéaire, traduisant des relations complexes entre variables, et ce afin d’améliorer les performances prédictives. Le détail de la méthodologie se trouve à la racine de ce repo [Documentation et résultats HepavirAlgo](https://gitlab.com/healthdatahub/boas/iplesp/hepaviralgo/-/blob/main/Documentation%20et%20r%C3%A9sultats%20HepavirAlgo.pdf?ref_type=heads).

Ensuite, par industrialisation de ces méthodes il est possible de les déployer rapidement à d'autres phénotypes que la simple détection des VHB et VHC chroniques (ici : cirrhoses, variant Delta et porteurs sains). Enfin, les codes pour requêter en R les bases Oracle, pré-traiter le SNDS, le transformer en une base unique, puis y appliquer un algorithme de machine learning, seront disponibles librement en open source pour la réutilisation à d'autres projets.

## Données utilisées :

En amont de la mise en open source des algorithmes HepavirAlgo, un travail d'investigation et d'évaluation des méthodes a eu lieu en population restreinte de manière similaire à [(Lam, 2023)](https://onlinelibrary.wiley.com/doi/full/10.1111/jvh.13788). En effet, avant de pouvoir appliquer ces algorithmes "en conditions réelles" au SNDS entier sur le portail de la CNAM, ils ont d'abord été établis et affiner en utilisant la cohorte HEPATHER (gold standard des véritables statuts de pathologies) appariée au SNDS. La documentation précise de l'entraînement du modèle et de ses performances est également disponible à la racine de ce repo. Cette documentation donne notamment plus de détails sur la _feature selection_ via des régressions Lasso, le _data management_ du SNDS, le choix de l'algorithme-package XGBoost et les performances par phénotype.

## Validation : 

Ces tableaux de performances sont à nouveau disponibles dans la [documentation détaillée](https://gitlab.com/healthdatahub/boas/iplesp/hepaviralgo/-/blob/main/Documentation%20et%20r%C3%A9sultats%20HepavirAlgo.pdf?ref_type=heads) expliquant l'entraînement du modèle. Il est fondamental de garder à l'esprit que ces métriques correspondent à la validation en **population restreinte** (données d’Hépather appariées avec le SNDS) et ne garantissent en rien la généralisation en population générale. Des travaux sont actuellement en cours pour vérifier la concordance de ces algorithmes en population générale avant publication complète des codes R.

|                       | Précision          |    Rappel     | F1-Score | Nb individus test
| :-------------------: | :----------------: | :-----------: |:--------:| :---------------:
| Autres (tous VHC, VHB guéris ou aigüe)|0,99|   0,98        | 0,98     | 2284
| VHB chroniques        | 0,95               |   0,97        | 0,96     | 881

|                       | Précision          |    Rappel     | F1-Score | Nb individus test
| :-------------------: | :----------------: | :-----------: |:--------:| :---------------:
| Autres (tous VHB, VHC guéris ou aigüe)|0,92|   0,84        | 0,88     | 1174
| VHC chroniques        | 0,91               |   0,95        | 0,93     | 1968


|                       | Précision          |    Rappel     | F1-Score | Nb individus test
| :-------------------: | :----------------: | :-----------: |:--------:| :---------------:
| Autres (VHB chro sans fibrose ou < F3)|0,95|   0,88        | 0,91     | 629
| VHB chro avec fibrose stade F4        |0,36|   0,82        | 0,51     | 55

|                       | Précision          |    Rappel     | F1-Score | Nb individus test
| :-------------------: | :----------------: | :-----------: |:--------:| :---------------:
| Autres (VHC chro sans fibrose ou < F3)|0,88|   0,84        | 0,86     | 1154
| VHC chro avec fibrose stade F4        |0,72|   0,76        | 0,74     | 611

## Date de dernière mise à jour :

Il s’agit de la version 1.0 pour l'implémentation R et Python : octobre 2023

## Maintenance

Maintenu par [Titouan Blaize](https://www.linkedin.com/in/titouan-blaize-34906781/)(titouan.blaize@health-data-hub.fr). Ce code est également en cours de passation à l’IPLESP (samuel.nilusmas@iplesp.upmc.fr et paul.burgat@iplesp.upmc.fr)

## Comment installer l'algorithme :

Une fois établie la méthodologie et les modèles de machine learning entraînés sur la cohorte HEPATHER, les coefficients et poids de ces modèles ont été exportés pour utilisation en population générale. Pour lancer les algorithmes sur le portail de la CNAM il suffit de :
- Lancer sa session Rstudio normalement depuis la plateforme CNAM en se plaçant dans un dossier/projet R
- Créer un fichier R à la racine du dossier/projet R - peu importe le nom donné à ce fichier - et y copier l'intégralité du contenu `installation_hepaviralgo.R` puis de le lancer. Ce programme va créer tout l'arborescence et le contenu des fichiers présents dans le dossier `algorithme`.
- Lancer le programme principal `hepaviralgo.R` en choisissant le phénotype d'intérêt, les années d'intérêt, et en ajustant _éventuellement_ le seuil de détection selon l'objectif souhaité (afin de calibrer précision, sensibilité et spécificité désirées)
Ce programme crée alors deux résultats : l'ensemble des scores de probabilité de pathologie pour l'ensemble des BEN_NIR_PSA retenus, et surtout la création des BEN_NIR_ANO correspondant à la cohorte finale d'intérêt.

## Comment utiliser l'algorithme :

Dans l’attente de 1) la mise à jour du package XGBoost sur la plateforme CNAM et 2) la validation en population générale de ces algorithmes, la mise à disposition des codes R/Oracle est en attente. Les codes ayant permis la validation interne en Python, qui est présentée dans [Documentation et résultats HepavirAlgo](https://gitlab.com/healthdatahub/boas/iplesp/hepaviralgo/-/blob/main/Documentation%20et%20r%C3%A9sultats%20HepavirAlgo.pdf?ref_type=heads), est cependant déjà disponible dans “poc python”.

## Support :

Point de contact HDH : dir.donnees-accompagnement@health-data-hub.fr 
Point de contact IPLESP : fabrice.carrat@iplesp.upmc.fr

## Contributions :

Ouvert aux propositions et issues.

## License :

Apache 2.0

## Autre :
Dans l’attente de la mise à jour du package XGBoost (0.82.1 actuellement sur la plateforme), une version prédictive illustrative avec une régression linéaire à été développée. Cette prédiction, qui n’est pas la méthodologie développée, permet cependant d’avoir déjà tout le code R-Oracle et notamment la partie data management du SNDS, pour plus d’informations sur ces codes merci de contacter les personnes maintenant l’algorithme.
